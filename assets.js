const DEBUG_TEXTURE = [];

for (var i = 0; i < 16; ++i)
{
	for (var j = 0; j < 16; ++j)
	{
		if ((i % 2 == 0 && j % 2 != 0) || (i % 2 != 0 && j % 2 == 0))
		{
			DEBUG_TEXTURE.push(255, 0, 255, 255);
		}
		else
		{
			DEBUG_TEXTURE.push(0, 0, 0, 255);
		}
	}
}

const Assets = 
{
	initialized: false,
	textures: [],
	textureData: [],
	wallShader: null,
	spriteShader: null,
	uiShader: null,
	spriteMesh: null,
	doorMesh: null,
	wallMesh: null,
	quadMesh: null, //For UI elements
	loadingAssets: 0,
	
	loadTextureFromFile: function(texObj, path, loadCallback)
	{
		++this.loadingAssets;

		var tex = gl.createTexture();

		//Generate an invalid debug texture
		gl.bindTexture(gl.TEXTURE_2D, tex);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 16, 16, 0, gl.RGBA, gl.UNSIGNED_BYTE, new Uint8Array(DEBUG_TEXTURE));
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);

		var img = new Image();
		img.src = path;
		img.onload = () => 
		{
			texObj.img = img;
			texObj.width = img.width;
			texObj.height = img.height;
			gl.bindTexture(gl.TEXTURE_2D, tex);
			gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, img);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);
			if (Object.keys(Assets.textureData).includes(texObj.name))
			{
				if (Object.keys(Assets.textureData[texObj.name]).includes("animation"))
				{
					//Animated textures may be non-power-of-two.
					//WebGL will complain unless we do this
					gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
					gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
				}
			}
			if (texObj.directory == "textures2") //Textures2 contains hud elements, non power of two.
			{
				gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
				gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
			}
			DebugLog("TEXTURE LOADED: " + path + "::" + texObj.name);

			--this.loadingAssets;
			if (loadCallback) loadCallback();
		}
		img.onerror = () => {
 			DebugLog("!!! FAILED TO LOAD TEXTURE: " + path);
		}
		return tex;
	},

	/**
	 * Retrieves the texture with the given name, and loads it asyncronously if it isn't loaded yet
	 * Name is relative to "textures" directory unless preceded by "textures2/".
	 * @param loadCallback Called when the texture is asyncronously loaded, or if it is already loaded.
	 */
	getTexture: function(path, loadCallback)
	{
		//If no directory is specified, textures/ is assumed
		if (path.search("textures/") == -1 && path.search("textures2/") == -1) path = "textures/".concat(path);
		var name = path.replace("textures/", "").replace("textures2/", "");

		for (var i = 0; i < this.textures.length; i++)
		{
			if (this.textures[i] === undefined) continue;
			if (this.textures[i].name === name)
			{
				if (loadCallback && this.textures[i].img) loadCallback();
				return this.textures[i];
			}
		}

		var newTex = {
			name:name, 
			width:0, 
			height:0, 
			img: undefined, 
			directory: "textures"
		};
		if (path.search("textures2/" != -1)) newTex.directory = "textures2";
		newTex.id = this.loadTextureFromFile(newTex, "assets/"+path+".png", loadCallback);

		this.textures.push(newTex);
		return newTex;
	},

	getTexturePromise: function(path)
	{
		return new Promise((resolve, reject) => {
			this.getTexture(path, resolve);
		});
	},

	clearTextures: function(filter)
	{
		for (var i = 0; i < this.textures.length; i++)
		{
			if (this.textures[i] === undefined) continue;
			if (this.textures[i].directory == filter)
			{
				gl.deleteTexture(this.textures[i].id);
				this.textures.splice(i, i+1);
			}
		}
	},

	getTextureProperty(texName, propName)
	{
		if (Assets.textureData[texName] !== undefined)
		{
			if (Assets.textureData[texName][propName] !== undefined)
			{
				return Assets.textureData[texName][propName];
			}
		}
		return null;
	},

	getFile: function(filePath)
	{
		++this.loadingAssets;
		return new Promise((resolve, reject) => {
			var req = new XMLHttpRequest();
			req.onreadystatechange = () => {
				if (req.readyState == 4)
				{
					--this.loadingAssets;
					if (req.status == 200)
					{
						resolve(req.responseText);
					}
					else
					{
						reject(Error("File at " + filePath + " could not be loaded!"));
					}
				}
			};
			req.open("GET", filePath, true);
			req.send();
		});
	},

	initTextures: function()
	{
		return this.getFile("assets/textures/TEXINFO.json").then((response) => {
			var textureData = JSON.parse(response);

			this.textureData = [];
			
			var keys = Object.keys(textureData);
			for (var i = 0; i < keys.length; i++)
			{
				this.textureData[keys[i]] = textureData[keys[i]];
			}

			this.initialized = true;
		});
	},

	initShaders: function()
	{
		const shaderFromFile = (name, vertName, fragName, callBack) => {
			return Promise.all([this.getFile("assets/shaders/" + vertName), this.getFile("assets/shaders/" + fragName)])
				.then((responseArray) => {
					const shader = new Shader(name, responseArray[0], responseArray[1]);
					DebugLog("SHADER " + name + " LOADED.");
					callBack(shader);
				});
		}

		return Promise.all([
			shaderFromFile("wall", "wall_vert.glsl", "wall_frag.glsl", shader => this.wallShader = shader),
			shaderFromFile("sprite", "sprite_vert.glsl", "sprite_frag.glsl", shader => this.spriteShader = shader),
			shaderFromFile("ui", "ui_vert.glsl", "ui_frag.glsl", shader => this.uiShader = shader)
		]);
	},

	init: function()
	{
		this.spriteMesh = new Mesh(new Uint8Array([
			0, 1, 2, 0, 2, 3
		]), new Float32Array([
			-1.0, -1.0, 0.0,
			+1.0, -1.0, 0.0,
			+1.0, +1.0, 0.0,
			-1.0, +1.0, 0.0
		]), new Float32Array([
			0.0, 1.0,
			1.0, 1.0,
			1.0, 0.0,
			0.0, 0.0
		]))

		this.doorMesh = new Mesh(new Uint8Array([
			2, 1, 0, 3, 2, 0,
			4, 5, 6, 4, 6, 7
		]), new Float32Array([
			-1.0, -1.0, -0.05,
			 1.0, -1.0, -0.05,
			 1.0,  1.0, -0.05,
			-1.0,  1.0, -0.05,
			-1.0, -1.0,  0.05,
			 1.0, -1.0,  0.05,
			 1.0,  1.0,  0.05,
			-1.0,  1.0,  0.05
		]), new Float32Array([
			0.0, 1.0,
			1.0, 1.0,
			1.0, 0.0,
			0.0, 0.0,
			0.0, 1.0,
			1.0, 1.0,
			1.0, 0.0,
			0.0, 0.0
		]), new Float32Array([
			0.0, 0.0, 1.0,
			0.0, 0.0, 1.0,
			0.0, 0.0, 1.0,
			0.0, 0.0, 1.0,
			0.0, 0.0, -1.0,
			0.0, 0.0, -1.0,
			0.0, 0.0, -1.0,
			0.0, 0.0, -1.0
		]));

		this.wallMesh = new Mesh(new Uint8Array([
			0, 1, 2, 0, 2, 3,
			6, 5, 4, 7, 6, 4,
			10, 9, 8, 11, 10, 8,
			12, 13, 14, 12, 14, 15
		]), new Float32Array([
			-1.0, -1.0, -1.0,
			-1.0, -1.0,  1.0,
			-1.0,  1.0,  1.0,
			-1.0,  1.0, -1.0,
			+1.0, -1.0, -1.0,
			+1.0, -1.0,  1.0,
			+1.0,  1.0,  1.0,
			+1.0,  1.0, -1.0,
			-1.0, -1.0, -1.0,
			 1.0, -1.0, -1.0,
			 1.0,  1.0, -1.0,
			-1.0,  1.0, -1.0,
			-1.0, -1.0, +1.0,
			 1.0, -1.0, +1.0,
			 1.0,  1.0, +1.0,
			-1.0,  1.0, +1.0
		]), new Float32Array([
			0.0, 1.0,
			1.0, 1.0,
			1.0, 0.0,
			0.0, 0.0,
			1.0, 1.0,
			0.0, 1.0,
			0.0, 0.0,
			1.0, 0.0,
			1.0, 1.0,
			0.0, 1.0,
			0.0, 0.0,
			1.0, 0.0,
			0.0, 1.0,
			1.0, 1.0,
			1.0, 0.0,
			0.0, 0.0
		]), new Float32Array([
			-1.0, 0.0, 0.0,
			-1.0, 0.0, 0.0,
			-1.0, 0.0, 0.0,
			-1.0, 0.0, 0.0,
			+1.0, 0.0, 0.0,
			+1.0, 0.0, 0.0,
			+1.0, 0.0, 0.0,
			+1.0, 0.0, 0.0,
			0.0, 0.0, -1.0,
			0.0, 0.0, -1.0,
			0.0, 0.0, -1.0,
			0.0, 0.0, -1.0,
			0.0, 0.0, +1.0,
			0.0, 0.0, +1.0,
			0.0, 0.0, +1.0,
			0.0, 0.0, +1.0,
		]));

		this.quadMesh = new Mesh(new Uint8Array([
			2, 1, 0, 0, 3, 2
		]), new Float32Array([
			0.0, 0.0, 0.0,
			1.0, 0.0, 0.0,
			1.0, 1.0, 0.0,
			0.0, 1.0, 0.0
		]));

		return Promise.all([this.initTextures(), this.initShaders()]);
	},
};

function Animation(frmWidth, frmHeight, texture, speed)
{
	this.sourceX = 0.0;
	this.sourceY = 0.0;
	this.sourceWidth = 1.0;
	this.sourceHeight = 1.0;
	this.frameWidth = frmWidth;
	this.frameHeight = frmHeight;
	this.index = 0;
	this.frames = null;
	this.animTimer = 0.0;
	this.animSpeed = speed;
	this.texture = texture;
	this.animated = false;
}
Animation.prototype = 
{
	update: function(deltaTime)
	{
		if (this.texture.img !== undefined && !this.animated)
		{
			this.animated = true;
			this.sourceWidth = this.frameWidth / this.texture.width;
			this.sourceHeight = this.frameHeight / this.texture.height;
			if (this.frames == null) //By default, the animation cycles through every possible frame in the image
			{
				this.frames = new Array((this.texture.height / this.frameHeight) * (this.texture.width / this.frameWidth));
				for (var i = 0; i < this.frames.length; i++)
				{
					this.frames[i] = i;
				}
			}
		}
		if (this.animated)
		{
			this.animTimer += deltaTime;
			if (this.animTimer > this.animSpeed)
			{
				this.animTimer = 0.0;
				this.index += 1;
				if (this.index >= this.frames.length) this.index = 0;
				const frame = this.frames[this.index];
				const frameCountX = (this.texture.width / this.frameWidth);
				const frameCountY = (this.texture.height / this.frameHeight);
				this.sourceX = (frame % frameCountX) * this.sourceWidth;
				this.sourceY = (Math.floor((frame * this.frameWidth) / this.texture.width) % frameCountY) * this.sourceHeight;
			}
		}
	}
};