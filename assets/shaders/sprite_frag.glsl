#version 100

precision mediump float;

uniform sampler2D u_texture;
uniform vec2 u_sourcePos;
uniform vec2 u_sourceSize;
uniform float u_fogStart;
uniform float u_fogLength;

varying vec2 v_texCoord;

void main()
{
    if (gl_FragCoord.z >= 0.999) discard;
    vec2 texCoord = vec2(v_texCoord.x, v_texCoord.y);
    texCoord *= u_sourceSize;
    texCoord += u_sourcePos;
    vec4 color = texture2D(u_texture, texCoord);
    float fog = 1.0 - clamp((gl_FragCoord.z - u_fogStart) / u_fogLength, 0.0, 1.0);
    color.xyz *= fog;
    if (color.a < 1.0) discard;
    gl_FragColor = color;
}