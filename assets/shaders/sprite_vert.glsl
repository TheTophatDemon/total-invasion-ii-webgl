#version 100

uniform mat4 u_modelMatrix;
uniform mat4 u_viewMatrix;
uniform mat4 u_projMatrix;

attribute vec4 aPosition;
attribute vec2 aTexCoord;

varying vec2 v_texCoord;

void main()
{
    vec4 pos = u_viewMatrix * u_modelMatrix * vec4(0.0, 0.0, 0.0, 1.0);
    pos += vec4(aPosition.xy, 0.0, 0.0);
    pos = u_projMatrix * pos;
    gl_Position = pos;
    v_texCoord = aTexCoord.xy;
}