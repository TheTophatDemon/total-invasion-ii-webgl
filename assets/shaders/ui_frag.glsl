#version 100

precision mediump float;

uniform sampler2D u_texture;
uniform vec2 u_sourcePos;
uniform vec2 u_sourceSize;
uniform vec4 u_color;

varying vec2 v_texCoord;

void main()
{
    vec2 texCoord = v_texCoord;
    texCoord *= u_sourceSize;
    texCoord += u_sourcePos;
    vec4 color = texture2D(u_texture, texCoord);
    color *= u_color;
    if (color.a <= 0.0) discard;
    gl_FragColor = color;
}