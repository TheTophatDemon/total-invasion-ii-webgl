#version 100

uniform mat4 u_matrix;

attribute vec4 aPosition;

varying vec2 v_texCoord;

void main()
{
    gl_Position = u_matrix * aPosition;
    v_texCoord = aPosition.xy;
}