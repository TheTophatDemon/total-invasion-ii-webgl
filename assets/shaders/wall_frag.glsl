#version 100

precision mediump float;

varying vec2 v_texCoord;
varying vec4 v_normal;

uniform sampler2D u_texture;
uniform vec3 u_ambientLight;
uniform vec2 u_sourcePos;
uniform vec2 u_sourceSize;
uniform float u_fogStart;
uniform float u_fogLength;

void main()
{
    if (gl_FragCoord.z >= 0.999) discard;
    vec2 texCoord = vec2(v_texCoord.x, v_texCoord.y);
    vec4 sampled = texture2D(u_texture, (texCoord * u_sourceSize) + u_sourcePos);
    if (sampled.a < 1.0) discard;
    vec4 color = vec4(u_ambientLight.x, u_ambientLight.y, u_ambientLight.z, 1.0);
    if (u_ambientLight.x != 1.0 || u_ambientLight.y != 1.0 || u_ambientLight.z != 1.0)
    {
        float dp = 1.0;
        if (v_normal.x != 0.0)
            dp = 0.5;
        if (v_normal.z > 0.0)
            dp = 0.0;
        color.xyz += dp * (vec3(1.0)-u_ambientLight);
    }
    color *= sampled;
    float fog = 1.0 - clamp((gl_FragCoord.z - u_fogStart) / u_fogLength, 0.0, 1.0);
    color.xyz *= fog;
    gl_FragColor = color;
}