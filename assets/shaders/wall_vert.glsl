#version 100

attribute vec4 aPosition;
attribute vec2 aTexCoord;
attribute vec3 aNormal;

uniform mat4 u_vpMatrix;

varying vec2 v_texCoord;
varying vec4 v_normal;

void main()
{
    gl_Position = u_vpMatrix * aPosition;
    v_texCoord = aTexCoord.xy;
    v_normal = vec4(aNormal, 0.0);
}