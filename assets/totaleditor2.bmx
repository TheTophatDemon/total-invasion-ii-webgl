Import MaxGui.Drivers

Incbin "grid.png"
Incbin "lines.png"

AppTitle = "Total Editor II Version 2"
Graphics 768, 576
HideMouse()

Type TTexture
	Field img:TImage
	Field name:String
End Type
Function MakeTexture:TTexture(img:TImage, name:String)
	Local t:TTexture = New TTexture
	t.img = img
	t.name = name
	Return t
End Function

Global IMG_GRID:TImage = LoadImage("incbin::grid.png", 0)
Global IMG_LINES:TImage = LoadAnimImage("incbin::lines.png", 16, 16, 0, 28, 0)
Global gTextures:TList = CreateList()

ChangeDir "textures/"
dir = ReadDir(CurrentDir())
nf:String = ""
Repeat
	nf = NextFile(dir)
	If nf = "." Or nf = ".." Then Continue
	If nf = "" Then Exit
	If LoadPixmap(nf) <> Null Then
		Local texName:String = RemoveTags(StripAll(nf))
		Local skip:Byte = False
		For Local tex:TTexture = EachIn gTextures
			If tex.name = texName Then
				skip = True
				Exit
			EndIf
		Next
		If skip = True Then Continue
		ListAddLast gTextures, MakeTexture(LoadImage(nf,0), texName)
		Print "TEXTURE LOADED: " + texName + " / " + StripAll(nf)
	EndIf
Forever
CloseDir dir
ChangeDir "../"

Global gCameraX:Int = 0
Global gCameraY:Int = 0
Global gCameraY2:Int = 0
Global gCameraSpeed:Int = 6
Global gCursorX:Int = 0
Global gCursorY:Int = 0
Global gTextureIndex:Int = 0
Global gThingFlag:Byte = 0
Global gThingAngle:Byte = 0

Global gMapNumber:Byte = 1
Global gEpisodeNumber:Byte = 1

Global gSelectedTexture:TTexture = TTexture(gTextures.First())
Global gSelectedIndex:Int = 0

Global gTiles:TList = CreateList()
Global gSectors:TList = CreateList()
Global gThings:TList = CreateList()

Global gGhost:TTile = New TTile
gGhost.texture = gSelectedTexture

Const STATE_EDITOR:Int = 0
Const STATE_TEXTURE:Int = 1
Global gState:Int = STATE_EDITOR

Const EDITING_TILES:Int = 0
Const EDITING_USECTORS:Int = 1
Const EDITING_DSECTORS:Int = 2
Const EDITING_THINGS:Int = 3
Global gEditing:Int = EDITING_TILES

Global gMakingSector:Int = False
Global gNewSector:TSector = Null

Global gLargestId:Int = 0

Type TTile
	Field x:Int = 0
	Field y:Int = 0
	Field orientation:Int = 0
	Field texture:TTexture = Null
	Field textureIndex:Int = 0
	Field flag:Byte = 0
	Field id:Byte = 0
	Method Render()
		SetColor 255, 255, 255
		SetAlpha 1.0
		If gEditing <> EDITING_TILES Then SetAlpha 0.3
		SetScale 1.0, 1.0
		DrawSubImageRect texture.img, x, y, 16, 16, 0, 0, 64, 64
		
		If flag = 0 Then
			DrawImage IMG_LINES, x, y, orientation
		Else
			DrawImage IMG_LINES, x, y, orientation + 13
		EndIf
		If id > 0 Then
			DrawText id, x + 4, y + 2
		EndIf
	End Method
EndType

Type TSector
	Field class:Int = 0
	Field x:Int = 0
	Field y:Int = 0
	Field texture:TTexture = Null
	Field textureIndex:Int = 0
	Field lightLevel:Byte = 255
	Method Render()		SetColor lightLevel, lightLevel, lightLevel
		SetAlpha 1.0
		If (class = 0 And gEditing = EDITING_USECTORS) Or (class = 1 And gEditing = EDITING_DSECTORS) Or gEditing = EDITING_TILES Or gEditing = EDITING_THINGS Then SetAlpha 0.0
		DrawSubImageRect texture.img, x, y, 16, 16, 0, 0, 64, 64
		SetScale 1.0, 1.0
	End Method
EndType

Type TThing
	Field x:Int = 0
	Field y:Int = 0
	Field flag:Byte = 0
	Field angle:Byte = 0
	Field texture:TTexture = Null
	Field textureIndex:Int = 0
	Method Render()
		SetColor 255, 255, 255
		SetAlpha 1.0
		SetScale 1.0, 1.0
		DrawSubImageRect texture.img, x, y, 16, 16, 0, 0, 64, 64
		DrawImage IMG_LINES, x, y, 26
		SetColor 255, 255, 0
		DrawText flag, x + 4, y + 2
		SetColor 255, 255, 255
		DrawLine x+8,y+8, x+8+Cos(angle*45)*8, y+8+Sin(angle*45)*8
	End Method
EndType

Const MENU_FILE = 0
Const MENU_SAVE = 64
Const MENU_LOAD = 65
Const MENU_NEW = 66
Const MENU_OLD = 1
Const MENU_OLDSAVE = 128
Const MENU_OLDLOAD = 129

Global gFileWindow:TGadget = CreateWindow("Total Editor 2", 0, 0, 256, 256, Null, WINDOW_DEFAULT)

Global gEpNumField:TGadget = CreateTextField(48, 0, 64, 16, gFileWindow, 0)
SetGadgetText(gEpNumField, "1")
Local epLabel:TGadget = CreateLabel("Episode:", 0, 0, 64, 16, gFileWindow, LABEL_LEFT)
Global gMapNumField:TGadget = CreateTextField(48, 16, 64, 16, gFileWindow, 0)
SetGadgetText(gMapNumField, "1")
Local mapLabel:TGadget = CreateLabel("Map:", 0, 16, 64, 16, gFileWindow, LABEL_LEFT)

Global gFileMenu:TGadget = CreateMenu("&File", MENU_FILE, WindowMenu(gFileWindow))
Local saveMenu:TGadget = CreateMenu("&Save", MENU_SAVE, gFileMenu)
Local loadMenu:TGadget = CreateMenu("&Load", MENU_LOAD, gFileMenu)
Local newMenu:TGadget = CreateMenu("&New", MENU_NEW, gFileMenu)
Global gOldMenu:TGadget = CreateMenu("&Old Format", MENU_OLD, WindowMenu(gFileWindow))
Local saveOldMenu:TGadget = CreateMenu("&Save", MENU_OLDSAVE, gOldMenu)
Local loadOldMenu:TGadget = CreateMenu("&Load", MENU_OLDLOAD, gOldMenu)

UpdateWindowMenu(gFileWindow)

While Not AppTerminate() And Not KeyDown(KEY_ESCAPE)
	Cls
	
	Local t:TTile = Null
	Local s:TSector = Null
	Local th:TThing = Null
	
	If gState = STATE_EDITOR Then
		
		If KeyHit(KEY_1) Then 
			gEditing = EDITING_TILES; FlushMouse(); FlushKeys();
		ElseIf KeyHit(KEY_2) Then 
			gEditing = EDITING_USECTORS; FlushMouse(); FlushKeys();
		ElseIf KeyHit(KEY_3) Then 
			gEditing = EDITING_DSECTORS; FlushMouse(); FlushKeys();
		ElseIf KeyHit(KEY_4) Then 
			gEditing = EDITING_THINGS; FlushMouse(); FlushKeys();
		EndIf
				
		If KeyHit(KEY_TAB) Then 
			gState = STATE_TEXTURE
			FlushMouse()
			FlushKeys()
		EndIf
		
		If KeyDown(KEY_D) Then
			gCameraX :+ gCameraSpeed
		ElseIf KeyDown(KEY_A) Then
			gCameraX :- gCameraSpeed
		EndIf
		
		If KeyDown(KEY_W) Then
			gCameraY :- gCameraSpeed
		ElseIf KeyDown(KEY_S) Then
			gCameraY :+ gCameraSpeed
		EndIf
		
		If KeyDown(KEY_LSHIFT) Then
			gCameraSpeed = 12
		Else
			gCameraSpeed = 6
		EndIf
		
		If KeyHit(KEY_R) Then
			gCameraX = 0
			gCameraY = 0
		EndIf
		
		gCursorX = ((MouseX() + gCameraX)/16)*16
		gCursorY = ((MouseY() + gCameraY)/16)*16
		
		If gEditing = EDITING_TILES Then
			
			gGhost.texture = gSelectedTexture
			gGhost.textureIndex = gSelectedIndex
			gGhost.x = gCursorX
			gGhost.y = gCursorY
			
			'If KeyHit(KEY_E) Then gGhost.orientation :+ 1
			'If KeyHit(KEY_Q) Then gGhost.orientation :- 1
			'If gGhost.orientation > 12 Then gGhost.orientation = 0
			'If gGhost.orientation < 0 Then gGhost.orientation = 12
			
			If (MouseDown(MOUSE_LEFT) Or MouseDown(MOUSE_RIGHT)) And KeyDown(KEY_LSHIFT) Then
				For t = EachIn gTiles
					If t.x = gCursorX And t.y = gCursorY Then
						ListRemove gTiles, t
					EndIf
				Next
			ElseIf MouseDown(MOUSE_LEFT) And Not KeyDown(KEY_LSHIFT) Then
				For t = EachIn gTiles
					If t.x = gCursorX And t.y = gCursorY Then
						ListRemove gTiles, t
					EndIf
				Next
				Local tile:TTile = New TTile
				tile.x = gGhost.x
				tile.y = gGhost.y
				tile.orientation = gGhost.orientation
				tile.texture = gGhost.texture
				tile.textureIndex = gGhost.textureIndex
				tile.flag = gGhost.flag
				tile.id = gGhost.id
				ListAddLast gTiles, tile
				
			ElseIf MouseHit(MOUSE_RIGHT) Then
				For t = EachIn gTiles
					If t.x = gCursorX And t.y = gCursorY Then
						gGhost.orientation = t.orientation
						gGhost.texture = t.texture
						gGhost.textureIndex = t.textureIndex
						gSelectedTexture = t.texture
						gSelectedIndex = t.textureIndex
						gGhost.flag = t.flag
						gGhost.id = t.id
					EndIf
				Next
			EndIf
			
			If KeyHit(KEY_F) Then gGhost.flag :- 1
			If KeyHit(KEY_G) Then gGhost.flag :+ 1
			If KeyHit(KEY_Z) Then gGhost.id :- 1
			If KeyHit(KEY_X) Then gGhost.id :+ 1
			
		ElseIf gEditing = EDITING_USECTORS Or gEditing = EDITING_DSECTORS
			gGhost.texture = gSelectedTexture
			gGhost.textureIndex = gSelectedIndex
			gGhost.x = gCursorX
			gGhost.y = gCursorY
			gGhost.orientation = 0
			
			gmx = MouseX() + gCameraX
			gmy = MouseY() + gCameraY
			If (MouseDown(MOUSE_RIGHT) Or MouseDown(MOUSE_LEFT)) And KeyDown(KEY_LSHIFT) Then
				For s = EachIn gSectors
					If (gCursorX = s.x And gCursorY = s.y)  And ((s.class = 0 And gEditing = EDITING_DSECTORS) Or (s.class = 1 And gEditing = EDITING_USECTORS)) Then

						ListRemove gSectors, s
					EndIf
				Next
			EndIf
			
			If MouseDown(MOUSE_LEFT) And Not KeyDown(KEY_LSHIFT) Then
				For s = EachIn gSectors
					If (gCursorX >= s.x And gCursorY >= s.y And gCursorX < s.x+16 And gCursorY < s.y+16)  And ((s.class = 0 And gEditing = EDITING_DSECTORS) Or (s.class = 1 And gEditing = EDITING_USECTORS)) Then
						ListRemove gSectors, s
					EndIf
				Next
					gNewSector = New TSector
					gNewSector.x = ((MouseX() + gCameraX)/16)*16
					gNewSector.y = ((MouseY() + gCameraY)/16)*16
					gNewSector.texture = gSelectedTexture
					gNewSector.textureIndex = gSelectedIndex
					gNewSector.class = 0
					If gEditing = EDITING_USECTORS Then gNewSector.class = 1
					ListAddLast gSectors, gNewSector
					gNewSector = Null
			ElseIf MouseDown(MOUSE_RIGHT) And Not KeyDown(KEY_LSHIFT) Then
				For s = EachIn gSectors
					If (gCursorX+8 >= s.x And gCursorY+8 >= s.y And gCursorX+8 <= s.x+16 And gCursorY+8 <= s.y+16)  And ((s.class = 0 And gEditing = EDITING_DSECTORS) Or (s.class = 1 And gEditing = EDITING_USECTORS)) Then
						gSelectedTexture = s.texture
						gSelectedIndex = s.textureIndex
					EndIf
				Next
			EndIf
			
			If gNewSector <> Null Then
				'gNewSector.x1 = gCursorX
				'gNewSector.y1 = gCursorY
			EndIf
		ElseIf gEditing = EDITING_THINGS
			If KeyDown(KEY_LSHIFT) Then
				If MouseHit(MOUSE_LEFT) Or MouseHit(MOUSE_RIGHT) Then
					For th = EachIn gThings
						If gCursorX = th.x And gCursorY = th.y Then
							ListRemove gThings, th
						EndIf
					Next
				EndIf
			Else
				If MouseHit(MOUSE_LEFT) Then
					For th = EachIn gThings
						If gCursorX = th.x And gCursorY = th.y Then
							ListRemove gThings, th
						EndIf
					Next
					Local thing:TThing = New TThing
					thing.x = gCursorX
					thing.y = gCursorY
					thing.flag = gThingFlag
					thing.angle = gThingAngle
					thing.texture = gSelectedTexture
					thing.textureIndex = gSelectedIndex
					ListAddLast gThings, thing
				ElseIf MouseHit(MOUSE_RIGHT)
					For th = EachIn gThings
						If gCursorX = th.x And gCursorY = th.y Then
							gThingFlag = th.flag
							gSelectedTexture = th.texture
							gSelectedIndex = th.textureIndex
							gThingAngle = th.angle
						EndIf
					Next
				EndIf
			EndIf
			If KeyHit(KEY_F) Then gThingFlag :- 1
			If KeyHit(KEY_G) Then gThingFlag :+ 1
			If KeyHit(KEY_RIGHT) Then gThingAngle :+ 1
			If KeyHit(KEY_LEFT) Then gThingAngle :- 1
			gThingAngle = gThingAngle Mod 8
		EndIf
		
		SetBlend ALPHABLEND
		SetOrigin -gCameraX, -gCameraY
		TileImage IMG_GRID, 0, 0
		
		For s = EachIn gSectors
			If s.class = 0 Then s.Render()
		Next
		
		gLargestId = 0
		For t = EachIn gTiles
			If t.id > gLargestId And t.id <> 255 Then gLargestId = t.id
			t.Render()
		Next
		
		For s = EachIn gSectors
			If s.class = 1 Then s.Render()
		Next
		
		For th = EachIn gThings
			th.Render()
		Next
		SetColor 255, 0, 255
		SetAlpha 1.0
		If gEditing = EDITING_TILES Then DrawImage IMG_LINES, gGhost.x, gGhost.y, gGhost.orientation
		If gEditing = EDITING_USECTORS Or gEditing = EDITING_DSECTORS Then
			SetColor 255, 0, 255 
			SetAlpha 0.5
			DrawRect gCursorX, gCursorY, 16, 16
		EndIf
		If gEditing = EDITING_THINGS Then
			DrawImage IMG_LINES, gCursorX, gCursorY, 26
		EndIf
		
		If gEditing <> EDITING_THINGS Then
			SetColor 255, 255, 255
			SetAlpha 1.0
			DrawImage gSelectedTexture.img, gCameraX + 32, gCameraY + 24
			DrawText "CURRENT TEXTURE:", gCameraX + 4, gCameraY + 4
			DrawText "FLAG: " + gGhost.flag, gCameraX + 4, gCameraY + 96
			DrawText "ID: " + gGhost.id, gCameraX + 4, gCameraY + 128
			DrawText "LARGEST ID: " + gLargestId, gCameraX + 4, gCameraY + 160
		Else
			SetColor 255, 255, 255
			SetAlpha 1.0
			DrawImage gSelectedTexture.img, gCameraX + 32, gCameraY + 24
			DrawLine gCameraX + 64, gCameraY + 56, gCameraX+64+Cos(gThingAngle*45)*32, gCameraY+56+Sin(gThingAngle*45)*32
			DrawText "FLAG: " + gThingFLag, gCameraX + 4, gCameraY + 96
		EndIf
		
		SetColor 255, 255, 255
		SetAlpha 1.0
		DrawText "E" + gEpisodeNumber + "M" + gMapNumber, gCameraX + 764 - TextWidth("E" + gEpisodeNumber + "M" + gMapNumber), gCameraY + 4
		DrawText "X: " + gCursorX + " Y: " + gCursorY, gCameraX + 4, gCameraY + 560
		Select gEditing
			Case EDITING_TILES
				DrawText "EDITING: TILES", gCameraX+384-TextWidth("EDITING: TILES")/2, gCameraY
			Case EDITING_THINGS
				DrawText "EDITING: THINGS", gCameraX+384-TextWidth("EDITING: THINGS")/2, gCameraY
			Case EDITING_USECTORS
				DrawText "EDITING: CEILINGS", gCameraX+384-TextWidth("EDITING: CEILINGS")/2, gCameraY
			Case EDITING_DSECTORS
				DrawText "EDITING: FLOORS", gCameraX+384-TextWidth("EDITING: FLOORS")/2, gCameraY
		EndSelect
	ElseIf gState = STATE_TEXTURE
		If KeyHit(KEY_TAB) Then gState = STATE_EDITOR
		
		If KeyDown(KEY_W) Then gCameraY2 :- 2
		If KeyDown(KEY_S) Then gCameraY2 :+ 2
		If gCameraY2 < 0 Then gCameraY2 = 0
		
		gCursorX = ((MouseX())/64)*64
		gCursorY = ((MouseY() + gCameraY2)/64)*64
		
		If MouseHit(MOUSE_LEFT) Then
			Local index:Int = (gCursorX/64) + ((gCursorY/64) * (768/64))
			gSelectedTexture = TTexture(gTextures.ValueAtIndex(index))
			gSelectedIndex = index
			gState = STATE_EDITOR
			FlushMouse()
		EndIf
		
		SetBlend ALPHABLEND
		SetOrigin 0, -gCameraY2
		SetColor 255, 255, 255
		SetAlpha 1.0
		
		For Local i:Int = 0 To CountList(gTextures) - 1
			DrawImageRect TTexture(gTextures.ValueAtIndex(i)).img, (i * 64) Mod 768, Floor((i * 64) / 768) * 64, 64, 64
		Next
		
		SetAlpha 0.5
		DrawRect gCursorX, gCursorY, 64, 64
	EndIf
	
	Local wText:String = GadgetText(gFileWindow)
	While PollEvent() <> 0
		If EventID() = EVENT_MENUACTION Then
			Select EventData()
				Case MENU_SAVE
					SaveMap()
				Case MENU_LOAD
					LoadMap()
				Case MENU_NEW
					ClearList gTiles
					ClearList gSectors
					ClearList gThings
				Case MENU_OLDSAVE
					SetGadgetText gFileWindow, "LOADING..."
					SaveMap_OLD()
					SetGadgetText gFileWindow, wText
				Case MENU_OLDLOAD
					SetGadgetText gFileWindow, "LOADING..."
					LoadMap_OLD()
					SetGadgetText gFileWindow, wText
			End Select
		ElseIf EventID() = EVENT_WINDOWCLOSE Then
			End
		ElseIf EventID() = EVENT_GADGETACTION Then
			Select EventSource()
				Case gEpNumField
					gEpisodeNumber = Int(GadgetText(gEpNumField))
				Case gMapNumField
					gMapNumber = Int(GadgetText(gMapNumField))
			End Select
		EndIf
	Wend
		
	Flip
	GCCollect
Wend

Function SaveMap()
	Local wText:String = GadgetText(gFileWindow)
	SetGadgetText gFileWindow, "LOADING..."
	
	'Find width and height
	Local mapWidth:Int = 0
	Local mapHeight:Int = 0
	Local minX:Int = 99999
	Local minY:Int = 99999
	Local maxX:Int = -99999
	Local maxY:Int = -99999
	For Local t:TTile = EachIn gTiles
		If t.x < minX Then minX = t.x End
		If t.x > maxX Then maxX = t.x End
		If t.y < minY Then minY = t.y End
		If t.y > maxY Then maxY = t.y End
	Next
	For Local s:TSector = EachIn gSectors
		If s.x < minX Then minX = s.x End
		If s.x > maxX Then maxX = s.x End
		If s.y < minY Then minY = s.y End
		If s.y > maxY Then maxY = s.y End
	Next
	mapWidth = maxX-minX+1
	mapHeight = maxY-minY+1
	
	Local file:TStream = WriteFile("maps/E" + gEpisodeNumber + "M" + gMapNumber + ".ti2")
	WriteLine file, mapWidth + "," + mapHeight
	
	
	SetGadgetText gFileWindow, wText
End Function

Function LoadMap()
	Local wText:String = GadgetText(gFileWindow)
	SetGadgetText gFileWindow, "LOADING..."
	
	SetGadgetText gFileWindow, wText
End Function

Function SaveMap_OLD()
	Local file:TStream = WriteFile("maps/"+"E" + gEpisodeNumber + "M" + gMapNumber + ".ti")
		
	WriteLine file, "TILES"
	WriteLine file, CountList(gTiles)
	For Local t:TTile = EachIn gTiles
		WriteLine file, t.x + "," + t.y + "," + t.orientation + "," + TTexture(gTextures.ValueAtIndex(t.textureIndex)).name+ "," + t.flag + "," + t.id
	Next
	WriteLine file, "SECTORS"
	WriteLine file, CountList(gSectors)
	For Local s:TSector = EachIn gSectors
		WriteLine file, s.x + "," + s.y + "," + (s.x+16) + "," + (s.y+16) + "," + s.class + "," + TTexture(gTextures.ValueAtIndex(s.textureIndex)).name
	Next
	WriteLine file, "THINGS"
	WriteLine file, CountList(gThings)
	For th:TThing = EachIn gThings
		WriteLine file, th.x + "," + th.y + "," + th.flag + "," + TTexture(gTextures.ValueAtIndex(th.textureIndex)).name + "," + th.angle
	Next
	
	CloseStream file
End Function

Function LoadMap_OLD()
		ClearList gTiles
		ClearList gSectors
		ClearList gThings
		
		file:TStream = ReadFile("maps/"+"E" + gEpisodeNumber + "M" + gMapNumber + ".ti")
		Local line:String = ""
		ReadLine(file)
		Local tileCount:Int = Int(ReadLine(file))
		For i = 0 To tileCount - 1
			line = ReadLine(file)
			Local args:String[] = line.Split(",")
			Local tl:TTile = New TTile
			tl.x = Int(args[0])
			tl.y = Int(args[1])
			tl.orientation = 0
			For Local j:Int = 0 To CountList(gTextures) - 1
				If TTexture(gTextures.ValueAtIndex(j)).name = RemoveTags(args[3]) Then
					tl.textureIndex = j
				EndIf
			Next
			tl.texture = TTexture(gTextures.ValueAtIndex(tl.textureIndex))
			tl.flag = Int(args[4])
			tl.id = Int(args[5])
			ListAddLast gTiles, tl
		Next
		ReadLine(file)
		Local sectorCount:Int = Int(ReadLine(file))
		For i = 0 To sectorCount - 1
			line = ReadLine(file)
			args = line.Split(",")
			Local sec:TSector = New TSector
			sec.x = Int(args[0])
			sec.y = Int(args[1])
			sec.class = Int(args[4])
			For j = 0 To CountList(gTextures) - 1
				If TTexture(gTextures.ValueAtIndex(j)).name = RemoveTags(args[5]) Then
					sec.textureIndex = j
				EndIf
			Next
			sec.texture = TTexture(gTextures.ValueAtIndex(sec.textureIndex))
			ListAddLast gSectors, sec
		Next
		ReadLine(file)
		Local thingCount:Int = Int(ReadLine(file))
		For i = 0 To thingCount - 1
			line = ReadLine(file)
			args = line.Split(",")
			Local thg:TThing = New TThing
			thg.x = Int(args[0])
			thg.y = Int(args[1])
			thg.flag = Int(args[2])
			For j = 0 To CountList(gTextures) - 1
				If TTexture(gTextures.ValueAtIndex(j)).name = RemoveTags(args[3]) Then
					thg.textureIndex = j
				EndIf
			Next
			thg.texture = TTexture(gTextures.ValueAtIndex(thg.textureIndex))
			If Len(args) > 4 Then thg.angle = Int(args[4])
			ListAddLast gThings, thg
		Next
		
		CloseStream file
End Function

Function RemoveTags:String(texName:String)
	Local animTag:Int = Instr(texName, "_animated")
	If animTag <> 0 Then texName = Left(texName, animTag-1)
	Local invisibleTag:Int = Instr(texName, "_invisible")
	If invisibleTag <> 0 Then texName = Left(texName, invisibleTag-1)
	Local solidTag:Int = Instr(texName, "_notsolid")
	If solidTag <> 0 Then texName = Left(texName, solidTag-1)
	Return texName
End Function
