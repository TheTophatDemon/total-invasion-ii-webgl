function Camera()
{
	this.lightDirection = [-1.0, 0.0, 0.0];
	this.ambientLight = [0.75, 0.75, 0.75];
	this.fogStart = 0.99;
	this.fogLength = 0.01;
	
	this.perspMat = mat4.create();
	mat4.perspective(this.perspMat, Math.PI / 2.0, gAspectRatio, 0.1, 10000.0);
	this.viewMat = mat4.create();
	this.viewPerspMat = mat4.create();
	this.invViewPerspMat = mat4.create();
}
Camera.prototype = 
{
	pointInView: function(x, y, z, r)
	{
		var vec = vec3.fromValues(x, y, z);
		vec3.transformMat4(vec, vec, this.viewMat);
		
		if (vec3.squaredLength(vec) < 24.0)
		{
			return true;
		}
		
		var leftVec = vec3.clone(vec);
		leftVec[0] -= r;
		vec3.transformMat4(leftVec, leftVec, this.perspMat);
		
		var rightVec = vec3.clone(vec);
		rightVec[0] += r;
		vec3.transformMat4(rightVec, rightVec, this.perspMat);
		
		return (Math.abs(leftVec[0]) < gAspectRatio || Math.abs(rightVec[0]) < gAspectRatio)
			&& (Math.abs(leftVec[1]) < 1.0 || Math.abs(rightVec[1]) < 1.0)
			&& (Math.abs(leftVec[2]) < 1.0 || Math.abs(rightVec[2]) < 1.0);
	},
	alignWithPlayer: function(x, y, z, yaw)
	{
		mat4.identity(this.viewMat);
		mat4.lookAt(this.viewMat, vec3.fromValues(x, y, z), 
			vec3.fromValues(x + Math.cos(yaw), y, z + Math.sin(yaw)), vec3.fromValues(0.0, 1.0, 0.0));
		mat4.multiply(this.viewPerspMat, this.perspMat, this.viewMat);
		mat4.invert(this.invViewPerspMat, this.viewPerspMat);
	}
}