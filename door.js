function Door(texName, pos, mov, yaw, wall)
{
	Entity.call(this, pos[0], pos[1], pos[2]);
	this.originalPos = vec3.create();
	vec3.copy(this.originalPos, pos);
	this.texture = Assets.getTexture(texName);
	this.movement = mov;
	this.setYaw(yaw);
	this.wall = wall; //The wall object that occupies this door's space and represents its collision body
	this.partners = new Array();
	this.state = 0; //0: Closed, 1: Opening, 2: Waiting, 3: Closing
	this.stateTimer = 0.0;
	this.stayOpen = false;
	this.openTime = 0.5;
	this.openSpeed = 1.8 / this.openTime; //distance divided by time
	this.mesh = Assets.doorMesh;
}

Door.prototype = 
{
	open: function()
	{
		if (this.state == 0)
		{
			this.state = 1;
			for (var i = 0; i < this.partners.length; i++)
			{
				this.partners[i].open();
			}
		}
	},
	update: function(deltaTime)
	{
		if (this.state > 0)
		{
			this.stateTimer += deltaTime;
			switch (this.state)
			{
				case 1: //Opening
					var spd = deltaTime * this.openSpeed;
					this.translate(this.movement[0] * spd, this.movement[1] * spd, this.movement[2] * spd);
					if (this.stateTimer > this.openTime) 
					{
						this.stateTimer = 0.0;
						this.state++;
						
						//Align door/secret wall with desired open position
						vec3.copy(this.pos, this.originalPos);
						var m = vec3.create();
						vec3.copy(m, this.movement);
						vec3.scale(m, m, this.openSpeed * this.openTime);
						vec3.add(this.pos, this.pos, m);
						
						//this.wall.solid = false;
					}
					break;
				case 2: //Waiting
					if (this.stateTimer > 1.0 && this.stayOpen === false)
					{
						var obstructed = false;
						for (var i = 0; i < gWorld.entities.length; i++)
						{
							var ent = gWorld.entities[i];
							if (ent.radius > 0.0)
							{
								obstructed = ent.collidesWithBox(this.originalPos[0], this.originalPos[1], this.originalPos[2], 1.0);
								if (obstructed) break;
							}
						}
						if (!obstructed)
						{
							this.stateTimer = 0.0;
							this.state++;
							//this.wall.solid = true;
						}
					}
					break;
				case 3: //Closing
					var spd = -deltaTime * this.openSpeed;
					this.translate(this.movement[0] * spd, this.movement[1] * spd, this.movement[2] * spd);
					if (this.stateTimer > this.openTime)
					{
						this.stateTimer = 0.0;
						this.state = 0;
						vec3.copy(this.pos, this.originalPos);
					}
					break;
			}
		}
		this.wall.x = this.pos[0];
		this.wall.y = this.pos[1];
		this.wall.z = this.pos[2];
	},
	draw: function()
	{
		Entity.prototype.draw.call(this);
		if (!this.visible) return;
		
		//Assets.wallShader.bind();
		//Assets.doorMesh.bind();
		
		var mvpMat = mat4.create();
		mat4.multiply(mvpMat, gWorld.camera.viewPerspMat, this.mwMatrix);
		
		gl.uniformMatrix4fv(Assets.wallShader.getUniformLocation("u_vpMatrix"), false, mvpMat);
		gl.uniform3f(Assets.wallShader.getUniformLocation("u_lightDirection"), gWorld.camera.lightDirection[0], gWorld.camera.lightDirection[1], gWorld.camera.lightDirection[2]);
		gl.uniform3f(Assets.wallShader.getUniformLocation("u_ambientLight"), gWorld.camera.ambientLight[0], gWorld.camera.ambientLight[1], gWorld.camera.ambientLight[2]);
		gl.uniform1f(Assets.wallShader.getUniformLocation("u_fogStart"), gWorld.camera.fogStart);
		gl.uniform1f(Assets.wallShader.getUniformLocation("u_fogLength"), gWorld.camera.fogLength);
		
		gl.bindTexture(gl.TEXTURE_2D, this.texture.id);
		gl.uniform1i(Assets.wallShader.getUniformLocation("u_texture"), this.texture.id);
		
		gl.uniform1f(Assets.wallShader.getUniformLocation("u_fogStart"), gWorld.camera.fogStart);
		gl.uniform1f(Assets.wallShader.getUniformLocation("u_fogLength"), gWorld.camera.fogLength);
		
		gl.drawElements(gl.TRIANGLES, this.mesh.indexCount, gl.UNSIGNED_BYTE, 0);
	}
}
Object.setPrototypeOf(Door.prototype, Entity.prototype);