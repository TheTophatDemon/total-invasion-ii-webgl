function Mesh(iArray, vArray, uvArray, nArray, cArray)
{
	this.vertexBuffer = this.makeBuffer(vArray);
	this.indexCount = iArray.length;
	this.triangleCount = this.indexCount / 3;
	this.indexBuffer = this.makeElementBuffer(iArray);
	this.vertexSize = 3;
	
	if (uvArray !== undefined) this.uvBuffer = this.makeBuffer(uvArray);
	if (nArray !== undefined) this.normalBuffer = this.makeBuffer(nArray);
	if (cArray !== undefined) this.colorBuffer = this.makeBuffer(cArray);
}
Mesh.prototype = 
{
	bind: function()
	{
		gl.bindBuffer(gl.ARRAY_BUFFER, this.vertexBuffer);
		gl.vertexAttribPointer(POS_ATTR_LOC, this.vertexSize, gl.FLOAT, false, 0, 0);
		if (this.uvBuffer != null)
		{
			gl.bindBuffer(gl.ARRAY_BUFFER, this.uvBuffer);
			gl.vertexAttribPointer(UV_ATTR_LOC, 2, gl.FLOAT, false, 0, 0);
		}
		if (this.normalBuffer != null)
		{
			gl.bindBuffer(gl.ARRAY_BUFFER, this.normalBuffer);
			gl.vertexAttribPointer(NOR_ATTR_LOC, 3, gl.FLOAT, false, 0, 0);
		}
		if (this.colorBuffer != null)
		{
			gl.bindBuffer(gl.ARRAY_BUFFER, this.colorBuffer);
			gl.vertexAttribPointer(COL_ATTR_LOC, 3, gl.FLOAT, false, 0, 0);
		}
		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.indexBuffer);
	},
	destroy: function()
	{
		gl.deleteBuffer(this.vertexBuffer);
		gl.deleteBuffer(this.indexBuffer);
		if (this.uvBuffer != null) gl.deleteBuffer(this.uvBuffer);
		if (this.normalBuffer != null) gl.deleteBuffer(this.normalBuffer);
		if (this.colorBuffer != null) gl.deleteBuffer(this.colorBuffer);
	},
	makeBuffer: function(array)
	{
		var buf = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, buf);
		gl.bufferData(gl.ARRAY_BUFFER, array, gl.STATIC_DRAW);
		return buf;
	},
	makeElementBuffer: function(array)
	{
		var buf = gl.createBuffer();
		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, buf);
		gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, array, gl.STATIC_DRAW);
		return buf;
	}
};

//Mesh and shader are same for all sprites
function Sprite(texture)
{
	this.texture = texture;
	this.animated = false;
	this.animation = null;
}
Sprite.prototype = 
{
	update: function(deltaTime)
	{
		if (this.animated && this.animation != null)
		{
			this.animation.update(deltaTime);
		}
	},
	prepareRender: function()
	{
		Assets.spriteShader.bind();
		Assets.spriteMesh.bind();
	},
	draw: function(modelMatrix)
	{
		//Assets.spriteShader.bind();
		//Assets.spriteMesh.bind();
		
		gl.uniformMatrix4fv(Assets.spriteShader.getUniformLocation("u_modelMatrix"), false, modelMatrix);
		gl.uniformMatrix4fv(Assets.spriteShader.getUniformLocation("u_viewMatrix"), false, gWorld.camera.viewMat);
		gl.uniformMatrix4fv(Assets.spriteShader.getUniformLocation("u_projMatrix"), false, gWorld.camera.perspMat);
		
		gl.bindTexture(gl.TEXTURE_2D, this.texture.id);
		gl.uniform1i(Assets.spriteShader.getUniformLocation("u_texture"), this.texture.id);
		if (this.animation != null)
		{
			gl.uniform2f(Assets.spriteShader.getUniformLocation("u_sourcePos"), this.animation.sourceX, this.animation.sourceY);
			gl.uniform2f(Assets.spriteShader.getUniformLocation("u_sourceSize"), this.animation.sourceWidth, this.animation.sourceHeight);
		}
		else
		{
			gl.uniform2f(Assets.spriteShader.getUniformLocation("u_sourcePos"), 0.0, 0.0);
			gl.uniform2f(Assets.spriteShader.getUniformLocation("u_sourceSize"), 1.0, 1.0);
		}
		gl.uniform1f(Assets.spriteShader.getUniformLocation("u_fogStart"), gWorld.camera.fogStart);
		gl.uniform1f(Assets.spriteShader.getUniformLocation("u_fogLength"), gWorld.camera.fogLength);
		
		gl.drawElements(gl.TRIANGLES, Assets.spriteMesh.indexCount, gl.UNSIGNED_BYTE, 0);
	}
}