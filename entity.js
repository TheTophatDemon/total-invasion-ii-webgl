function Entity(x, y, z)
{
	this.pos = vec3.fromValues(x, y, z);
	this.dirty = true;
	this.visible = true;
	this.yaw = 0.0;
	this.radius = 1.0;
	this.viewRadius = 1.0;
	this.mwMatrix = null;
}

Entity.prototype = 
{
	update:function(deltaTime)
	{
		
	},
	setPos: function(x, y, z)
	{
		this.pos[0] = x;
		this.pos[1] = y;
		this.pos[2] = z;
		this.dirty = true;
	},
	translate: function(x, y, z)
	{
		this.pos[0] += x;
		this.pos[1] += y;
		this.pos[2] += z;
		this.dirty = true;
	},
	setYaw: function(angle)
	{
		this.yaw = angle;
		this.dirty = true;
	},
	rotate: function(angle)
	{
		this.yaw += angle;
		this.dirty = true;
	},
	draw:function()
	{
		if (this.dirty)
		{
			this.dirty = false;
			this.mwMatrix = mat4.create();
			mat4.fromTranslation(this.mwMatrix, this.pos);
			mat4.rotateY(this.mwMatrix, this.mwMatrix, this.yaw);
		}
		if (Object.getPrototypeOf(this) !== Player)
		{
			this.visible = gWorld.camera.pointInView(this.pos[0], this.pos[1], this.pos[2], this.viewRadius);
		}
	},
	tryMove: function(dx, dz)
	{
		if (!gKeyboard[81]) //Q
		{
			//Collide with the map
			var odx = dx;
			var odz = dz;
			var ldx = dx;
			var ldz = dz;
			for (var zz = 0; zz < gWorld.map.height; zz++)
			{
				for (var xx = 0; xx < gWorld.map.width; xx++)
				{
					if (gWorld.map.walls[zz][xx] != null)
					{
						if (gWorld.map.walls[zz][xx].solid == true)
						{
							var tx = gWorld.map.walls[zz][xx].x;
							var ty = gWorld.map.walls[zz][xx].y;
							var tz = gWorld.map.walls[zz][xx].z;
							if (this.pos[0] - this.radius + dx < tx + 1.0
								&& this.pos[0] + this.radius + dx > tx - 1.0
								&& this.pos[1] - this.radius      < ty + 1.0
								&& this.pos[1] + this.radius      > ty - 1.0
								&& this.pos[2] - this.radius + dz < tz + 1.0
								&& this.pos[2] + this.radius + dz > tz - 1.0)
							{
								var px = Math.max(Math.min(this.pos[0], tx + 1.0), tx - 1.0) - tx;
								var pz = Math.max(Math.min(this.pos[2], tz + 1.0), tz - 1.0) - tz;
								var ndx = (tx + px + (this.radius * Math.sign(px))) - this.pos[0];
								var ndz = (tz + pz + (this.radius * Math.sign(pz))) - this.pos[2];
								if (Math.abs(px) > Math.abs(pz))
								{
									dx = ndx;
								}
								else if (Math.abs(px) < Math.abs(pz))
								{
									dz = ndz;
								}
								else
								{
									/*
										When it is unclear as to which axis the entity should be moved along to avoid collision for this wall,
										pick one based off of the desired movement direction. If other walls do not make the choice for you,
										then this choice will be applied by default.
									*/
									if (Math.abs(dx) > Math.abs(dz))
									{
										ldx = ndx;
									}
									else
									{
										ldz = ndz;
									}
								}
							}
						}
					}
				}
			}
			if (dx == odx && dz == odz)
			{
				dx = ldx;
				dz = ldz;
			}
			//Collide with props
			for (var i = 0; i < gWorld.entities.length; i++)
			{
				var ent = gWorld.entities[i];
				if (Object.getPrototypeOf(ent) === Prop.prototype)
				{
					if (ent.radius <= 0.0) continue;
					var diff = vec3.create();
					vec3.subtract(diff, ent.pos, vec3.fromValues(this.pos[0] + dx, this.pos[1], this.pos[2] + dz));
					var dist = vec3.length(diff);
					var pushDist = (this.radius + ent.radius) - dist;
					if (pushDist > 0.0)
					{
						//GameUI.showMessage("BANG!", 0.1);
						dx -= (diff[0] / dist) * pushDist;
						dz -= (diff[2] / dist) * pushDist;
					}
				}
			}
		}
		this.translate(dx, 0.0, dz);
	},
	collidesWithBox: function(bx, by, bz, br)
	{
		var proj = vec3.fromValues(
			Math.max(Math.min(bx + br, this.pos[0]), bx - br),
			Math.max(Math.min(by + br, this.pos[1]), by - br),
			Math.max(Math.min(bz + br, this.pos[2]), bz - br)
		);
		var diff = vec3.create();
		vec3.subtract(diff, proj, this.pos);
		var dist = vec3.length(diff);
		if (dist < this.radius)
		{
			return true;
		}
		return false;
	},
	getTouchingWalls: function(flag)
	{
		var array = new Array();
		var ix = Math.floor(this.pos[0] / 2.0);
		var iz = Math.floor(this.pos[2] / 2.0);
		var tr = Math.ceil(this.radius / 2.0);
		var tLeft = Math.max(0, ix - tr);
		var tTop = Math.max(0, iz - tr);
		var tRight = Math.min(gWorld.map.width - 1, ix + tr);
		var tBottom = Math.min(gWorld.map.height - 1, iz + tr);
		for (var j = tTop; j <= tBottom; j++)
		{
			for (var i = tLeft; i <= tRight; i++)
			{
				if (gWorld.map.walls[j][i] != null)
				{
					if (gWorld.map.walls[j][i].flag == flag)
					{
						if (gWorld.map.collidesWithBox(gWorld.map.walls[j][i].x, 0.0, gWorld.map.walls[j][i].z, 1.0))
						{
							array.push(gWorld.map.walls[j][i]);
						}
					}
				}
			}
		}
		return array;
	}
}