function TexGroup(texName, firstElement)
{
	this.mesh = null;
	this.texName = texName;
	this.children = [];
	this.children.push(firstElement);
	this.animation = null;
	const texData = Assets.textureData[texName];
	if (texData !== undefined)
	{
		if (texData.animation !== undefined)
		{
			this.animation = new Animation(texData.animation.frameWidth || 64, texData.animation.frameHeight || 64, Assets.getTexture(texName), texData.animation.speed || 0.125);
		}
	}
}

function GameMap()
{
	this.width = 0;
	this.height = 0;
	this.walls = null;
	this.wallGroups = null;
	this.floors = null;
	this.floorGroups = null;
	this.doors = [];
	this.secretWalls = [];
	this.shader = null;
	this.minX = Number.MAX_VALUE;
	this.minZ = Number.MAX_VALUE;
	this.maxX = Number.MIN_VALUE;
	this.maxZ = Number.MIN_VALUE;
}
GameMap.prototype = 
{
	load: function(episodeNum, mapNum)
	{
		Assets.getFile("assets/maps/E" + episodeNum + "M" + mapNum + ".ti").then((file) => this.parse(file));
	},
	parse: function(file)
	{
		return new Promise((resolve, reject) => 
		{
			if (this.wallGroups != null && this.wallGroups !== undefined) //Destroy wallgroup meshes from previous map
			{
				for (var i = 0; i < this.wallGroups.length; i++)
				{
					this.wallGroups[i].mesh.destroy();
				}
			}
			if (this.floorGroups != null && this.floorGroups !== undefined)
			{
				for (var i = 0; i < this.floorGroups.length; i++)
				{
					this.floorGroups[i].mesh.destroy();
				}
			}

			Assets.clearTextures("textures"); //Only clears textures from the 'textures' folder, not 'textures2'

			const lines = file.split('\n');
			const numTiles = parseInt(lines[1]);
			const tilesPos = 2;
			const numFloors = parseInt(lines[2 + numTiles + 1]);
			const floorsPos = 2 + numTiles + 2;
			const numEnts = parseInt(lines[2 + numTiles + 2 + numFloors + 1]);
			const entsPos = 2 + numTiles + 2 + numFloors + 2;
			
			this.wallGroups = new Array();
			this.floorGroups = new Array();
			
			this.minX = 99999; this.minZ = 99999;
			this.maxX = -99999; this.maxZ = -99999;
			//Find maximum boundaries of the map
			for (var n = 0; n < numTiles; n++)
			{
				const data = lines[n+tilesPos].split(',');
				var x = parseInt(data[0]) / 16;
				var z = parseInt(data[1]) / 16;
				if (x < this.minX) this.minX = x;
				if (x > this.maxX) this.maxX = x;
				if (z < this.minZ) this.minZ = z;
				if (z > this.maxZ) this.maxZ = z;
			}
			for (var n = 0; n < numFloors; n++)
			{
				const data = lines[n+floorsPos].split(',');
				var x = parseInt(data[0]) / 16;
				var z = parseInt(data[1]) / 16;
				if (x < this.minX) this.minX = x;
				if (x > this.maxX) this.maxX = x;
				if (z < this.minZ) this.minZ = z;
				if (z > this.maxZ) this.maxZ = z;
			}
			this.width = this.maxX - this.minX + 1;
			this.height = this.maxZ - this.minZ + 1;
			
			//Load walls
			this.walls = new Array();
			for (var i = 0; i < this.height; i++)
			{
				this.walls[i] = new Array();
			}
			for (var n = 0; n < numTiles; n++)
			{
				const data = lines[n+tilesPos].split(',');
				var ix = (parseInt(data[0]) / 16) - this.minX;
				var iz = (parseInt(data[1]) / 16) - this.minZ;
				var x = ix * 2.0;
				var z = iz * 2.0;
				var flag = parseInt(data[4]); //flag
				var id = parseInt(data[5]);
				this.walls[iz][ix] = {ix:ix, iz:iz, x:x, y:0.0, z:z, texName: data[3], 
					flag: flag, id: id,
					seeThru: false, solid:true, door: null};
				if (Assets.textureData[data[3]] !== undefined)
				{
					this.walls[iz][ix].seeThru = Assets.textureData[data[3]].seeThru; //Determines whether enemies can see through this wall
					this.walls[iz][ix].solid = Assets.textureData[data[3]].solid || true; //Determines whether actors can walk through this wall
				}
				//Assign wall group
				if (data[3] != "invisible" && flag != 5)
				{
					this.addToTexGroupArray(this.wallGroups, this.walls[iz][ix]);
				}
				switch(flag)
				{
					case 6:
					case 11:
					case 12:
					case 13:
						this.walls[iz][ix].solid = false;
						break;
					case 4:
						gWorld.addEntity(new Teleporter(x, 0.0, z, this.walls[iz][ix].id));
						this.walls[iz][ix] = null;
						break;
					case 1:
					case 8:
					case 2:
					case 9:
						var mov = vec3.create();
						if (Assets.getTextureProperty(data[3], "verticalDoor") === true)
						{
							mov[1] = 1.0;
						}
						else if (flag == 1 || flag == 8)
						{
							mov[0] = -1.0;
						}
						else if (flag == 2 || flag == 9)
						{
							mov[2] = 1.0;
						}
						var yaw = (flag == 2 || flag == 9) ? Math.PI/2.0 : 0.0;
						var door = new Door(data[3], vec3.fromValues(x, 0.0, z), mov, yaw, this.walls[iz][ix]);
						this.doors.push(door);
						this.walls[iz][ix].door = door;
						//If another door is adjacent to it, then they should open together.
						for (var i = -1; i <= 1; i++)
						{
							for (var j = -1; j <= 1; j++)
							{
								var xx = ix + i;
								var zz = iz + j;
								if (xx < 0 || zz < 0 || xx >= this.width || zz >= this.height) continue;
								if (this.walls[zz][xx] != null)
								{
									if (this.walls[zz][xx].door != null)
									{
										door.partners.push(this.walls[zz][xx].door);
										this.walls[zz][xx].door.partners.push(door);
										break;
									}
								}
							}
						}
						break;
					case 5:
						var mov = vec3.create();
						switch(id)
						{
							case 0:
								mov[0] = 1.0;
								break;
							case 1:
								mov[2] = -1.0;
								break;
							case 2:
								mov[0] = -1.0;
								break;
							case 3:
								mov[2] = 1.0;
								break;
						}
						var secretWall = new Door(data[3], vec3.fromValues(x, 0.0, z), mov, 0.0, this.walls[iz][ix]);
						secretWall.stayOpen = true;
						secretWall.openTime = 1.0;
						secretWall.openSpeed = 4.0 / secretWall.openTime;
						secretWall.mesh = Assets.wallMesh;
						this.secretWalls.push(secretWall);
						this.walls[iz][ix].door = secretWall;
						break;
				}
			}
			
			this.floors = new Array();
			for (var i = 0; i < this.height; i++)
			{
				this.floors[i] = new Array();
			}
			for (var n = 0; n < numFloors; n++)
			{
				const data = lines[n+floorsPos].split(',');
				var ix = (parseInt(data[0]) / 16) - this.minX;
				var iz = (parseInt(data[1]) / 16) - this.minZ;
				var x = ix * 2.0;
				var z = iz * 2.0;
				this.floors[iz][ix] = {
					x:x, z:z, 
					texName: data[5], 
					ceiling: parseInt(data[4]) == 1
				};
				
				this.addToTexGroupArray(this.floorGroups, this.floors[iz][ix]);
			}
			
			//Load entities in separate function
			this.placeEntities(entsPos, numEnts, lines);
			
			DebugLog("MAP LOADED WITH " + numEnts + " ENTITIES, " + numTiles + " WALLS, AND " + numFloors + " FLOORS.");
			for (var i = 0; i < this.wallGroups.length; i++)
			{
				this.buildWallGroupMesh(this.wallGroups[i]);
			}
			for (var i = 0; i < this.floorGroups.length; i++)
			{
				this.buildFloorGroupMesh(this.floorGroups[i]);
			}
		});
	},
	buildWallGroupMesh: function(wallGroup)
	{
		if (wallGroup.mesh != null) wallGroup.mesh.destroy();
		var vArray = new Array();
		var iArray = new Array();
		var uvArray = new Array();
		var nArray = new Array();
		var vertexCount = 0;
		for (var i = 0; i < wallGroup.children.length; i++)
		{
			var wall = wallGroup.children[i];
			if (wall !== undefined)
			{
				if (wall.flag != 0 && wall.flag != 3 && wall.flag != 5 && wall.flag != 10)
				{
					if (wall.flag == 7 && wall.id == 0)
					{
						//Build Horizontal Flat Wall
						vArray.push(wall.x-1.0, -1.0, wall.z-0.05,
									wall.x+1.0, -1.0, wall.z-0.05,
									wall.x+1.0, +1.0, wall.z-0.05,
									wall.x-1.0, +1.0, wall.z-0.05);
						uvArray.push(0.0, 1.0,
									1.0, 1.0,
									1.0, 0.0,
									0.0, 0.0);
						nArray.push(0.0, 0.0, 1.0,
									0.0, 0.0, 1.0,
									0.0, 0.0, 1.0,
									0.0, 0.0, 1.0);
						iArray.push(vertexCount + 2, vertexCount + 1, vertexCount + 0, vertexCount + 3, vertexCount + 2, vertexCount + 0);
						vertexCount += 4;
						vArray.push(wall.x-1.0, -1.0, wall.z+0.05,
									wall.x+1.0, -1.0, wall.z+0.05,
									wall.x+1.0, +1.0, wall.z+0.05,
									wall.x-1.0, +1.0, wall.z+0.05);
						uvArray.push(0.0, 1.0,
									1.0, 1.0,
									1.0, 0.0,
									0.0, 0.0);
						nArray.push(0.0, 0.0, 1.0,
									0.0, 0.0, 1.0,
									0.0, 0.0, 1.0,
									0.0, 0.0, 1.0);
						iArray.push(vertexCount + 0, vertexCount + 1, vertexCount + 2, vertexCount + 0, vertexCount + 2, vertexCount + 3);
						vertexCount += 4;
					}
					else if (wall.flag == 7 && wall.id == 1)
					{
						//Build Vertical Flat Wall
						vArray.push(wall.x-0.05, -1.0, wall.z-1.0,
									wall.x-0.05, -1.0, wall.z+1.0,
									wall.x-0.05, +1.0, wall.z+1.0,
									wall.x-0.05, +1.0, wall.z-1.0);
						uvArray.push(0.0, 1.0,
									1.0, 1.0,
									1.0, 0.0,
									0.0, 0.0);
						nArray.push(0.0, 0.0, 1.0,
									0.0, 0.0, 1.0,
									0.0, 0.0, 1.0,
									0.0, 0.0, 1.0);
						iArray.push(vertexCount + 0, vertexCount + 1, vertexCount + 2, vertexCount + 0, vertexCount + 2, vertexCount + 3);
						vertexCount += 4;
						vArray.push(wall.x+0.05, -1.0, wall.z-1.0,
									wall.x+0.05, -1.0, wall.z+1.0,
									wall.x+0.05, +1.0, wall.z+1.0,
									wall.x+0.05, +1.0, wall.z-1.0);
						uvArray.push(0.0, 1.0,
									1.0, 1.0,
									1.0, 0.0,
									0.0, 0.0);
						nArray.push(0.0, 0.0, 1.0,
									0.0, 0.0, 1.0,
									0.0, 0.0, 1.0,
									0.0, 0.0, 1.0);
						iArray.push(vertexCount + 2, vertexCount + 1, vertexCount + 0, vertexCount + 3, vertexCount + 2, vertexCount + 0);
						vertexCount += 4;
					}
				}
				else
				{
					//Find neighbors
					var ln = false; var rn = false; var tn = false; var bn = false;
					if (wall.ix > 0) {
						const nb = this.walls[wall.iz][wall.ix-1];
						if (nb != null){
							if (nb.flag == 0 && nb.texName != "invisible") ln = true;
						}
					}
					if (wall.ix < this.width-1) {
						const nb = this.walls[wall.iz][wall.ix+1];
						if (nb != null){
							if (nb.flag == 0 && nb.texName != "invisible") rn = true;
						}
					}
					if (wall.iz > 0){ 
						const nb = this.walls[wall.iz-1][wall.ix];
						if (nb != null){
							if (nb.flag == 0 && nb.texName != "invisible") tn = true;
						}
					}
					if (wall.iz < this.height-1) {
						const nb = this.walls[wall.iz+1][wall.ix];
						if (nb != null){
							if (nb.flag == 0 && nb.texName != "invisible") bn = true;
						}
					}
					//Construct sides of box
					if (!ln) //Left side
					{
						vArray.push(wall.x-1.0, -1.0, wall.z-1.0,
									wall.x-1.0, -1.0, wall.z+1.0,
									wall.x-1.0, +1.0, wall.z+1.0,
									wall.x-1.0, +1.0, wall.z-1.0);
						uvArray.push(0.0, 1.0,
									1.0, 1.0,
									1.0, 0.0,
									0.0, 0.0);
						nArray.push(-1.0, 0.0, 0.0,
									-1.0, 0.0, 0.0,
									-1.0, 0.0, 0.0,
									-1.0, 0.0, 0.0);
						iArray.push(vertexCount + 0, vertexCount + 1, vertexCount + 2, vertexCount + 0, vertexCount + 2, vertexCount + 3);
						vertexCount += 4;
					}
					if (!rn) //Right side
					{
						vArray.push(wall.x+1.0, -1.0, wall.z-1.0,
									wall.x+1.0, -1.0, wall.z+1.0,
									wall.x+1.0, +1.0, wall.z+1.0,
									wall.x+1.0, +1.0, wall.z-1.0);
						uvArray.push(1.0, 1.0,
									0.0, 1.0,
									0.0, 0.0,
									1.0, 0.0);
						nArray.push(+1.0, 0.0, 0.0,
									+1.0, 0.0, 0.0,
									+1.0, 0.0, 0.0,
									+1.0, 0.0, 0.0);
						iArray.push(vertexCount + 2, vertexCount + 1, vertexCount + 0, vertexCount + 3, vertexCount + 2, vertexCount + 0);
						vertexCount += 4;
					}
					if (!tn) //Back side
					{
						vArray.push(wall.x-1.0, -1.0, wall.z-1.0,
									wall.x+1.0, -1.0, wall.z-1.0,
									wall.x+1.0, +1.0, wall.z-1.0,
									wall.x-1.0, +1.0, wall.z-1.0);
						uvArray.push(1.0, 1.0,
									0.0, 1.0,
									0.0, 0.0,
									1.0, 0.0);
						nArray.push(0.0, 0.0, -1.0,
									0.0, 0.0, -1.0,
									0.0, 0.0, -1.0,
									0.0, 0.0, -1.0);
						iArray.push(vertexCount + 2, vertexCount + 1, vertexCount + 0, vertexCount + 3, vertexCount + 2, vertexCount + 0);
						vertexCount += 4;
					}
					if (!bn) //Front side
					{
						vArray.push(wall.x-1.0, -1.0, wall.z+1.0,
									wall.x+1.0, -1.0, wall.z+1.0,
									wall.x+1.0, +1.0, wall.z+1.0,
									wall.x-1.0, +1.0, wall.z+1.0);
						uvArray.push(0.0, 1.0,
									1.0, 1.0,
									1.0, 0.0,
									0.0, 0.0);
						nArray.push(0.0, 0.0, +1.0,
									0.0, 0.0, +1.0,
									0.0, 0.0, +1.0,
									0.0, 0.0, +1.0);
						iArray.push(vertexCount + 0, vertexCount + 1, vertexCount + 2, vertexCount + 0, vertexCount + 2, vertexCount + 3);
						vertexCount += 4;
					}
				}
			}
		}
		wallGroup.mesh = new Mesh(new Uint16Array(iArray), new Float32Array(vArray), new Float32Array(uvArray), new Float32Array(nArray));
		DebugLog("WALLGROUP MESH BUILT FOR " + wallGroup.texName + " WITH " + iArray.length + " INDICES AND " + vArray.length + " VERTICES.");
	},
	buildFloorGroupMesh: function(floorGroup)
	{
		if (floorGroup.mesh != null) floorGroup.mesh.destroy();
		var vArray = new Array();
		var iArray = new Array();
		var uvArray = new Array();
		var nArray = new Array();
		var vertexCount = 0;
		for (var i = 0; i < floorGroup.children.length; i++)
		{
			var floor = floorGroup.children[i];
			if (floor !== undefined)
			{
				if (floor.ceiling) //Ceiling
				{
					vArray.push(floor.x-1.0, +1.0, floor.z-1.0,
								floor.x+1.0, +1.0, floor.z-1.0,
								floor.x+1.0, +1.0, floor.z+1.0,
								floor.x-1.0, +1.0, floor.z+1.0);
					uvArray.push(0.0, 0.0,
								1.0, 0.0,
								1.0, 1.0,
								0.0, 1.0);
					nArray.push(0.0, -1.0, 0.0,
								0.0, -1.0, 0.0,
								0.0, -1.0, 0.0,
								0.0, -1.0, 0.0);
					iArray.push(vertexCount+0, vertexCount+1, vertexCount+2,
								vertexCount+0, vertexCount+2, vertexCount+3);
					vertexCount += 4;
				}
				else //Floor
				{
					vArray.push(floor.x-1.0, -1.0, floor.z-1.0,
								floor.x+1.0, -1.0, floor.z-1.0,
								floor.x+1.0, -1.0, floor.z+1.0,
								floor.x-1.0, -1.0, floor.z+1.0);
					uvArray.push(0.0, 0.0,
								1.0, 0.0,
								1.0, 1.0,
								0.0, 1.0);
					nArray.push(0.0, +1.0, 0.0,
								0.0, +1.0, 0.0,
								0.0, +1.0, 0.0,
								0.0, +1.0, 0.0);
					iArray.push(vertexCount+2, vertexCount+1, vertexCount+0,
								vertexCount+3, vertexCount+2, vertexCount+0);
					vertexCount += 4;
				}
			}
		}
		floorGroup.mesh = new Mesh(new Uint16Array(iArray), new Float32Array(vArray), new Float32Array(uvArray), new Float32Array(nArray));
		DebugLog("FLOORGROUP MESH BUILT FOR " + floorGroup.texName + " WITH " + iArray.length + " INDICES AND " + vArray.length + " VERTICES.");
	},
	placeEntities: function(entsPos, numEnts, lines)
	{
		for (var i = 0; i < numEnts; i++)
		{
			const data = lines[entsPos + i].split(',');
			var x = (((parseInt(data[0]) / 16.0) - this.minX) * 2.0);
			var z = (((parseInt(data[1]) / 16.0) - this.minZ) * 2.0);
			const type = parseInt(data[2]);
			var angle = parseInt(data[4]) * (Math.PI / 4.0);
			switch(type)
			{
				case 0: //Player
					//DebugLog(x + "," + z);
					gWorld.player.setPos(x, gWorld.player.pos[1], z);
					gWorld.player.setYaw(angle);
					break;
				case 1: //Prop
					gWorld.addEntity(new Prop(data[3], x, 0.0, z, 1.0));
					break;
			}
		}
	},
	addToTexGroupArray: function(texGroupArray, item)
	{
		if (item.texName.search("memewall") != -1) item.texName = "memewall" + Math.floor(Math.random() * 7).toString();
		for (var i = 0; i < texGroupArray.length; i++)
		{
			if (texGroupArray[i].texName == item.texName)
			{
				texGroupArray[i].children.push(item);
				return null;
			}
		}

		//If there is not already a group for this texture, create one and return its name.
		texGroupArray.push(new TexGroup(item.texName, item));
		Assets.getTexture(item.texName);
		return item.texName;
	},
	update: function(deltaTime)
	{
		if (this.wallGroups != null)
		{
			for (var i = 0; i < this.wallGroups.length; i++)
			{
				var wg = this.wallGroups[i];
				if (wg === undefined) continue;
				if (wg.animation)
				{
					wg.animation.update(deltaTime);
				}
			}
		}
		if (this.floorGroups != null)
		{
			for (var i = 0; i < this.floorGroups.length; i++)
			{
				var fg = this.floorGroups[i];
				if (fg === undefined) continue;
				if (fg.animation)
				{
					fg.animation.update(deltaTime);
				}
			}
		}
		for (var i = 0; i < this.doors.length; i++)
		{
			this.doors[i].update(deltaTime);
		}
		for (var i = 0; i < this.secretWalls.length; i++)
		{
			this.secretWalls[i].update(deltaTime);
		}
	},
	draw: function(shader)
	{
		shader.bind();
		gl.uniformMatrix4fv(shader.getUniformLocation("u_vpMatrix"), false, gWorld.camera.viewPerspMat);
		gl.uniform3f(shader.getUniformLocation("u_lightDirection"), gWorld.camera.lightDirection[0], gWorld.camera.lightDirection[1], gWorld.camera.lightDirection[2]);
		gl.uniform3f(shader.getUniformLocation("u_ambientLight"), gWorld.camera.ambientLight[0], gWorld.camera.ambientLight[1], gWorld.camera.ambientLight[2]);
		gl.uniform1f(shader.getUniformLocation("u_fogStart"), gWorld.camera.fogStart);
		gl.uniform1f(shader.getUniformLocation("u_fogLength"), gWorld.camera.fogLength);
		if (this.wallGroups != null)
		{
			for (var i = 0; i < this.wallGroups.length; i++)
			{
				if (this.wallGroups[i] === undefined || this.wallGroups[i].mesh == null || this.wallGroups[i].children === undefined) 
				{
					//DebugLog("Somethings up with wallgroup " + i);
					continue;
				}
				if (Assets.textureData[this.wallGroups[i].texName] !== undefined)
				{
					if (Assets.textureData[this.wallGroups[i].texName].bright === true)
					{
						gl.uniform3f(shader.getUniformLocation("u_ambientLight"), 1.0, 1.0, 1.0);
					}
				}
				gl.bindTexture(gl.TEXTURE_2D, Assets.getTexture(this.wallGroups[i].texName).id);
				gl.uniform1i(shader.getUniformLocation("u_texture"), Assets.getTexture(this.wallGroups[i].texName).id);
				if (this.wallGroups[i].animation != null)
				{
					gl.uniform2f(shader.getUniformLocation("u_sourcePos"), this.wallGroups[i].animation.sourceX, this.wallGroups[i].animation.sourceY);
					gl.uniform2f(shader.getUniformLocation("u_sourceSize"), this.wallGroups[i].animation.sourceWidth, this.wallGroups[i].animation.sourceHeight);
				}
				else
				{
					gl.uniform2f(shader.getUniformLocation("u_sourcePos"), 0.0, 0.0);
					gl.uniform2f(shader.getUniformLocation("u_sourceSize"), 1.0, 1.0);
				}
				this.wallGroups[i].mesh.bind();
				gl.drawElements(gl.TRIANGLES, this.wallGroups[i].mesh.indexCount, gl.UNSIGNED_SHORT, 0);
				gl.uniform3f(shader.getUniformLocation("u_ambientLight"), gWorld.camera.ambientLight[0], gWorld.camera.ambientLight[1], gWorld.camera.ambientLight[2]);
			}
		}
		if (this.floorGroups != null)
		{
			for (var i = 0; i < this.floorGroups.length; i++)
			{
				if (this.floorGroups[i] === undefined || this.floorGroups[i].mesh == null || this.floorGroups[i].children === undefined) 
				{
					//DebugLog("Somethings up with floorgroup " + i);
					continue;
				}
				if (Assets.textureData[this.floorGroups[i].texName] !== undefined)
				{
					if (Assets.textureData[this.floorGroups[i].texName].bright === true)
					{
						gl.uniform3f(shader.getUniformLocation("u_ambientLight"), 1.0, 1.0, 1.0);
					}
				}
				gl.bindTexture(gl.TEXTURE_2D, Assets.getTexture(this.floorGroups[i].texName).id);
				gl.uniform1i(shader.getUniformLocation("u_texture"), Assets.getTexture(this.floorGroups[i].texName).id);
				if (this.floorGroups[i].animation != null)
				{
					gl.uniform2f(shader.getUniformLocation("u_sourcePos"), this.floorGroups[i].animation.sourceX, this.floorGroups[i].animation.sourceY);
					gl.uniform2f(shader.getUniformLocation("u_sourceSize"), this.floorGroups[i].animation.sourceWidth, this.floorGroups[i].animation.sourceHeight);
				}
				else
				{
					gl.uniform2f(shader.getUniformLocation("u_sourcePos"), 0.0, 0.0);
					gl.uniform2f(shader.getUniformLocation("u_sourceSize"), 1.0, 1.0);
				}
				this.floorGroups[i].mesh.bind();
				gl.drawElements(gl.TRIANGLES, this.floorGroups[i].mesh.indexCount, gl.UNSIGNED_SHORT, 0);
				gl.uniform3f(shader.getUniformLocation("u_ambientLight"), gWorld.camera.ambientLight[0], gWorld.camera.ambientLight[1], gWorld.camera.ambientLight[2]);
			}
		}
		//Draw doors
		Assets.doorMesh.bind();
		for (var i = 0; i < this.doors.length; i++)
		{
			this.doors[i].draw();
		}
		//Draw secret walls
		Assets.wallMesh.bind();
		for (var i = 0; i < this.secretWalls.length; i++)
		{
			this.secretWalls[i].draw();
		}
	},
	rayQuery: function(x, z, angle, maxDist)
	{	
		//Act as though the x,y coordinates of walls are at their corners, not their centers.
		//This makes the calculations a little easier to wrap my head around, but requires
		//this little adjustment here.
		x += 1.0;
		z += 1.0;
	
		var dx = Math.cos(angle);
		var dz = Math.sin(angle);
		var tanA = dz / dx;
		var rInfV = {hitWall: null, dist: 0, rx: 0, rz: 0, steps: 0};
		var rInfH = {hitWall: null, dist: 0, rx: 0, rz: 0, steps: 0};
		
		//Test against vertical grid lines (moving horizontally)
		if (dx != 0.0)
		{
			var rdx = 0.0;
			var rdz = 0.0;
			if (dx > 0.0)
			{
				rInfV.rx = Math.ceil(x / 2.0) * 2.0;
				rdx = 2.0;	
			}
			else if (dx < 0.0)
			{
				rInfV.rx = Math.floor(x / 2.0) * 2.0;
				rdx = -2.0;
			}
			if (dz > 0.0)
			{
				rInfV.rz = z + Math.abs((rInfV.rx - x) * tanA);
				rdz = 2.0 * Math.abs(tanA);
			}
			else
			{
				rInfV.rz = z - Math.abs((rInfV.rx - x) * tanA);
				rdz = -2.0 * Math.abs(tanA);
			}
			
			rInfV.dist = Math.abs((rInfV.rx - x) / dx);
			while (rInfV.dist < maxDist)
			{
				var ix = Math.floor(rInfV.rx / 2.0);
				if (dx < 0) ix -= 1;
				var iz = Math.floor(rInfV.rz / 2.0);
				if (ix < 0 || ix >= this.width || iz < 0 || iz >= this.height) break;
				var wall = this.walls[iz][ix];
				if (wall != null)
				{
					if (wall.solid === true)
					{
						rInfV.hitWall = wall;
						break;
					}
				}
				rInfV.rx += rdx;
				rInfV.rz += rdz;
				rInfV.dist = Math.abs((rInfV.rx - x) / dx);
				rInfV.steps++;
			}
		}
		else
		{
			rInfV.dist = 99999;
		}
		
		//Test against horizontal grid lines (moving vertically)
		if (dz != 0.0)
		{
			var rdx = 0.0;
			var rdz = 0.0;
			if (dz > 0.0)
			{
				rInfH.rz = Math.ceil(z / 2.0) * 2.0;
				rdz = 2.0;
			}
			else if (dz < 0.0)
			{
				rInfH.rz = Math.floor(z / 2.0) * 2.0;
				rdz = -2.0;
			}
			if (tanA != 0.0)
			{
				if (dx > 0.0)
				{
					rInfH.rx = x + Math.abs((rInfH.rz - z) / tanA);
					rdx = 2.0 / Math.abs(tanA);
				}
				else
				{
					rInfH.rx = x - Math.abs((rInfH.rz - z) / tanA);
					rdx = -2.0 / Math.abs(tanA);
				}
			}
			else
			{
				rInfH.rx = x;
				rdx = 0.0;
			}
			
			rInfH.dist = Math.abs((rInfH.rz - z) / dz);
			while (rInfH.dist < maxDist)
			{
				var ix = Math.floor(rInfH.rx / 2.0);
				var iz = Math.floor(rInfH.rz / 2.0);
				if (dz < 0) iz -= 1;
				if (ix < 0 || ix >= this.width || iz < 0 || iz >= this.height) break;
				var wall = this.walls[iz][ix];
				if (wall != null)
				{
					if (wall.solid === true)
					{
						rInfH.hitWall = wall;
						break;
					}
				}
				rInfH.rx += rdx;
				rInfH.rz += rdz;
				rInfH.dist = Math.abs((rInfH.rz - z) / dz);
				rInfH.steps++;
			}
			
		}
		else
		{
			rInfH.dist = 99999;
		}
		
		//Whichever test that traveled the shortest distance gets returned
		if (rInfV.dist < rInfH.dist)
		{
			if (rInfV.dist < maxDist)
			{
				if (rInfV.hitWall != null) 
				{
					rInfV.rx -= 1.0;
					rInfV.rz -= 1.0;
					return rInfV;
				}
			}
		}
		else
		{
			if (rInfH.dist < maxDist)
			{
				if (rInfH.hitWall != null) 
				{
					rInfH.rx -= 1.0;
					rInfH.rz -= 1.0;
					return rInfH;
				}
			}
		}
		return null;
	}
};