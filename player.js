const PLY_WALKSPEED = 0.15;
const PLY_RUNSPEED = 0.25;
const PLY_FRICTION = 0.0017;
const PLY_TURNSPEED = 3.0;
const PLY_ACCELERATION = 1.25;

const KEY_FORWARD = 87;
const KEY_BACKWARD = 83;
const KEY_RIGHT = 68;
const KEY_LEFT = 65;
const KEY_TURNRIGHT = 39;
const KEY_TURNLEFT = 37;
const KEY_USE = 69;
const KEY_RUN = 16;

function Player(x, y, z)
{
	Entity.call(this, x, y, z);
	this.health = 100;
	this.forward = 0.0;
	this.strafe = 0.0;
	this.cosa = 0.0;
	this.sina = 0.0;
	this.radius = 0.5;
	this.visible = false;
}

Player.prototype = 
{
	update:function(deltaTime)
	{
		const maxSpeed = gKeyboard[KEY_RUN] ? PLY_RUNSPEED : PLY_WALKSPEED;
		if (gKeyboard[KEY_TURNRIGHT])
		{
			this.rotate(PLY_TURNSPEED * deltaTime);
		}
		else if (gKeyboard[KEY_TURNLEFT])
		{
			this.rotate(-PLY_TURNSPEED * deltaTime);
		}
		
		if (gMouseLeftButton)
		{
			this.rotate(gMouseDX * deltaTime * PLY_TURNSPEED);
		}
		
		if (gKeyboard[KEY_FORWARD] || (gMouseLeftButton && gMouseDY < -1))
		{
			this.forward += PLY_ACCELERATION * deltaTime;
			if (this.forward > maxSpeed) this.forward = maxSpeed;
		}
		else if (gKeyboard[KEY_BACKWARD])
		{
			this.forward -= PLY_ACCELERATION * deltaTime;
			if (this.forward < -maxSpeed) this.forward = -maxSpeed;
		}
		else
		{
			this.forward *= Math.pow(PLY_FRICTION, deltaTime);
			if (Math.abs(this.forward) < 0.01) this.forward = 0.0;
		}
		if (gKeyboard[KEY_RIGHT])
		{
			this.strafe += PLY_ACCELERATION * deltaTime;
			if (this.strafe > maxSpeed) this.strafe = maxSpeed;
		}
		else if (gKeyboard[KEY_LEFT])
		{
			this.strafe -= PLY_ACCELERATION * deltaTime;
			if (this.strafe < -maxSpeed) this.strafe = -maxSpeed;
		}
		else
		{
			this.strafe *= Math.pow(PLY_FRICTION, deltaTime);
			if (Math.abs(this.strafe) < 0.01) this.strafe = 0.0;
		}
		
		if (gKeyPress[KEY_USE])
		{
			var casts = [
				gWorld.map.rayQuery(this.pos[0], this.pos[2], this.yaw, 2.0)
			];
			for (var i = 0; i < casts.length; i++)
			{
				if (casts[i] != null)
				{
					if (casts[i].hitWall.door != null)
					{
						casts[i].hitWall.door.open();
					}
				}
			}
		}
		
		if (this.forward != 0)
		{
			GameUI.weaponBob += deltaTime * this.forward;
		}
		else if (this.strafe != 0)
		{
			GameUI.weaponBob += deltaTime * this.strafe;
		}
		
		this.cosa = Math.cos(this.yaw);
		this.sina = Math.sin(this.yaw);
		var dx = (this.cosa * this.forward) + (-this.sina * this.strafe);
		var dy = (this.sina * this.forward) + (this.cosa * this.strafe);
		this.tryMove(dx, dy);
		gWorld.camera.alignWithPlayer(this.pos[0], this.pos[1], this.pos[2], this.yaw);
	},
};
Object.setPrototypeOf(Player.prototype, Entity.prototype);