function Prop(texName, x, y, z, r)
{
	Entity.call(this, x, y, z);
	this.sprite = new Sprite(Assets.getTexture(texName));
	this.radius = r;
	this.viewRadius = Math.max(r, 1.0);
	if (Assets.textureData[texName] !== undefined) 
	{
		var anmData = Assets.textureData[texName].animation;
		if (anmData !== undefined)
		{
			this.sprite.animated = true;
			this.sprite.animation = new Animation(anmData.frameWidth || 64, 
				anmData.frameHeight || 64, this.sprite.texture, anmData.speed || 0.125);
		}
	}
}
Prop.prototype = 
{
	update: function(deltaTime)
	{
		this.sprite.update(deltaTime);
	},
	draw: function()
	{
		Entity.prototype.draw.call(this);
		if (!this.visible) return;
		this.sprite.draw(this.mwMatrix);
	}
};
Object.setPrototypeOf(Prop.prototype, Entity.prototype);
