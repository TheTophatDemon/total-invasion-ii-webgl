//WebGL Shader vertex attribute indexes
const POS_ATTR_LOC = 0; //Position
const UV_ATTR_LOC = 1; //UV Coordinates
const NOR_ATTR_LOC = 2; //Normals
const COL_ATTR_LOC = 3; //Colors

function Shader(name, vertSrc, fragSrc)
{
	this.name = name;
	this.uniformCache = [];

	//Initialize WebGL shader objects
	const getShaderObj = (src, type) => {
		const shader = gl.createShader(type);
		gl.shaderSource(shader, src);
		gl.compileShader(shader);

		const compiled = gl.getShaderParameter(shader, gl.COMPILE_STATUS);
		if (!compiled && !gl.isContextLost())
		{
			const error = gl.getShaderInfoLog(shader);
			DebugLog("SHADER ERRED: " + name + "\n" + error);
			gl.deleteShader(shader);
			return null;
		}

		return shader;
	}

	this.vertexShader = getShaderObj(vertSrc, gl.VERTEX_SHADER);
	this.fragShader = getShaderObj(fragSrc, gl.FRAGMENT_SHADER);

	//Create and link WebGL shader program
	this.program = gl.createProgram();
	gl.attachShader(this.program, this.vertexShader);
	gl.attachShader(this.program, this.fragShader);

	gl.bindAttribLocation(this.program, POS_ATTR_LOC, "aPosition");
	
	this.useUV = vertSrc.indexOf(" aTexCoord;") >= 0;
	if (this.useUV) gl.bindAttribLocation(this.program, UV_ATTR_LOC, "aTexCoord");
	
	this.useNormals = vertSrc.indexOf(" aNormal;") >= 0;
	if (this.useNormals) gl.bindAttribLocation(this.program, NOR_ATTR_LOC, "aNormal");

	this.useColors = vertSrc.indexOf(" aColor;") >= 0;
	if (this.useColors) gl.bindAttribLocation(this.program, COL_ATTR_LOC, "aColor");
	
	gl.linkProgram(this.program);
	const linked = gl.getProgramParameter(this.program, gl.LINK_STATUS);
	if (!linked && !gl.isContextLost())
	{
		const error = gl.getProgramInfoLog(this.program);
		DebugLog("PROGRAM FAILED TO LINK FOR SHADER " + name + ": " + error);
		gl.deleteProgram(this.program);
		gl.deleteProgram(this.fragShader);
		gl.deleteProgram(this.vertexShader);
	}
}

Shader.prototype.bind = function()
{
	gl.useProgram(this.program);
	gl.enableVertexAttribArray(POS_ATTR_LOC);
	if (this.useUV) gl.enableVertexAttribArray(UV_ATTR_LOC);
	if (this.useNormals) gl.enableVertexAttribArray(NOR_ATTR_LOC);
	if (this.useColors) gl.enableVertexAttribArray(COL_ATTR_LOC);
	//Screen will go black if there is no buffer bound to an enabled vertex attribute array!
}

Shader.prototype.getUniformLocation = function(name)
{
	for (const entry of this.uniformCache)
	{
		if (entry.key === name)
		{
			return entry.val;
		}
	}
	const newEntry = { key: name, val: gl.getUniformLocation(this.program, name) };
	this.uniformCache.push(newEntry);
	return newEntry.val;
}

Shader.prototype.destroy = function()
{
	gl.deleteProgram(this.program);
	gl.deleteProgram(this.vertexShader);
	gl.deleteProgram(this.fragShader);
}