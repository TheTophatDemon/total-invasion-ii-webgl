function Teleporter(x, y, z, id)
{
	Entity.call(this, x, y, z);
	this.id = id;
	this.lastPassenger = null;
	this.partner = null;
	var teleporterCount = 0;
	for (var i = 0; i < gWorld.entities.length; i++)
	{
		if (Object.getPrototypeOf(gWorld.entities[i]) === Teleporter.prototype)
		{
			teleporterCount++;
			if (gWorld.entities[i].id == id)
			{
				this.partner = gWorld.entities[i];
				gWorld.entities[i].partner = this;
			}
		}
	}
	if (this.partner === null && teleporterCount > 0)
	{
		DebugLog("!!! There is a teleporter without a partner !!!");
	}
}

Teleporter.prototype = 
{
	update: function(deltaTime)
	{
		if (this.partner === null) return;
		
		var foundLastPassenger = false;
		for (var i = 0; i < gWorld.entities.length; i++)
		{
			var ent = gWorld.entities[i];
			if (Object.getPrototypeOf(ent) === Player.prototype)
			{
				var diff = vec3.create();
				vec3.subtract(diff, ent.pos, this.pos);
				if (vec3.length(diff) < ent.radius + this.radius)
				{
					if (ent != this.lastPassenger)
					{
						vec3.copy(ent.pos, this.partner.pos);
						this.partner.lastPassenger = ent;
						GameUI.flashScreen(1.0, 0.0, 1.0, 1.0, 0.5);
					}
					else
					{
						foundLastPassenger = true;
					}
				}
			}
		}
		if (!foundLastPassenger) this.lastPassenger = null;
	}
};
Object.setPrototypeOf(Teleporter.prototype, Entity.prototype);