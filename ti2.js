//about:config -> privacy.file_unique_origin

//Redo asset loading
	//Put all textures into atlases
	//Embed texture info into js file
//Make Loading screen work properly
//Weird teleporter issue in E2M4
//Make game resolution independent
//UI
	//Make coordinates resolution independent?

//Refactoring
//Make constant for wall size in units

//Transformations apply right to left! Because function notation

var gl = null;

var gCanvas = null;
var gAspectRatio = 0;

var gTime = 0;
var gFps = 0;

var gWorld = null;

var gKeyboard = [];
var gKeyPress = [];
var gMouseX = 0;
var gMouseY = 0;
var gMouseDX = 0;
var gMouseDY = 0;
var gMousePX = 0;
var gMousePY = 0;
var gMouseLeftButton = false;
var gMouseRightButton = false;

window.onload = function() 
{
	gCanvas = document.getElementById("canvas");
	let animRequestId = null;
	let lastTime = Date.now();
	let fpsTicks = 0;
	let fpsTimer = 0;

	var loadShader = null;
	var loadTexture = null;

	init();

	function init()
	{
		gKeyboard.fill(false);
		gKeyPress.fill(false);
		
		gAspectRatio = gCanvas.clientWidth / gCanvas.clientHeight
		gCanvas.addEventListener('webglcontextlost', handleContextLost, false);
		
		gl = WebGLUtils.setupWebGL(gCanvas, {antialias:false, premultipliedAlpha: false, alpha: false});
		gl.enable(gl.DEPTH_TEST);
		gl.enable(gl.CULL_FACE);
		gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
		gl.clearColor(0.0, 0.0, 0.0, 1.0);
		
		//Load all assets, initialize subsystems, load the map, and begin the game loop in the correct sequence.
		Assets.init().then(() => {
			UI.init();
			GameUI.init();
			gWorld = new World();
			gWorld.loadMap(1, 1);
			tick();
		});

		//Draw loading screen while waiting using Quad and getTexture
		loadShader = new Shader("loadScreen", LOADING_SCREEN_SHADER_VSRC, LOADING_SCREEN_SHADER_FSRC);
		loadTexture = Assets.getTexture("../loadingscreen", () => {
			gl.viewport(0, 0, gCanvas.clientWidth, gCanvas.clientHeight);
			gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

			loadShader.bind();
			Assets.spriteMesh.bind();
			gl.bindTexture(gl.TEXTURE_2D, loadTexture.id);
			gl.uniform1i(loadShader.getUniformLocation("u_texture"), loadTexture.id);
			gl.drawElements(gl.TRIANGLES, Assets.spriteMesh.indexCount, gl.UNSIGNED_BYTE, 0);

			gl.flush();
		});
	}
	
	function tick()
	{
		var deltaTime = (Date.now() - lastTime) / 1000.0;
		lastTime = Date.now();
		fpsTimer += deltaTime;
		if (fpsTimer > 1.0)
		{
			gFps = fpsTicks;
			fpsTicks = 0.0;
			fpsTimer = 0.0;
		}
		else
		{
			fpsTicks++;
		}
		gTime += deltaTime;
		
		UI.update(deltaTime);
		gWorld.update(deltaTime);
		
		gl.viewport(0, 0, gCanvas.clientWidth, gCanvas.clientHeight);
		gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
		
		UI.clear();
		gl.enable(gl.DEPTH_TEST);
		gWorld.draw();

		gl.disable(gl.DEPTH_TEST);
		gl.enable(gl.BLEND);
		UI.draw();

		gKeyPress.fill(false);
		animRequestId = window.requestAnimFrame(tick, gCanvas);
	}

	function handleContextLost(e)
	{
		DebugLog("Lost the context, m8!!!!!");
		if (animRequestId) cancelAnimFrame(animRequestId);
	}
};

window.onkeydown = function(e)
{
	gKeyPress[e.keyCode] = !gKeyboard[e.keyCode];
	gKeyboard[e.keyCode] = true;
}

window.onkeyup = function(e)
{
	gKeyboard[e.keyCode] = false;
}

window.onmousedown = function(e)
{
	if (e.button == 0)
	{
		gMouseLeftButton = true;
	}
	else
	{
		gMouseRightButton = true;
	}
}

window.onmouseup = function(e)
{
	if (e.button == 0)
	{
		gMouseLeftButton = false;
	}
	else
	{
		gMouseRightButton = false;
	}
}

window.onmousemove = function(e)
{
	gMousePX = gMouseX;
	gMousePY = gMouseY;
	gMouseX = e.screenX;
	gMouseY = e.screenY;
	gMouseDX = gMouseX - gMousePX;
	gMouseDY = gMouseY - gMousePY;
}

function DebugLog(txt)
{
	if (console.log !== undefined)
	{
		console.log(txt);
	}
}