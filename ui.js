const LOADING_SCREEN_SHADER_VSRC = `
	#version 100

	attribute vec4 aPosition;
	varying vec2 v_texCoord;

	void main()
	{
		gl_Position = aPosition;
		v_texCoord = (aPosition.xy / 2.0) + vec2(0.5);
		v_texCoord.y = 1.0 - v_texCoord.y;
	}
`;

const LOADING_SCREEN_SHADER_FSRC = `
	#version 100

	precision mediump float;
	uniform sampler2D u_texture;
	varying vec2 v_texCoord;

	void main()
	{
		vec4 color = texture2D(u_texture, v_texCoord);
		gl_FragColor = color;
	}
`;

const WEAPON_X_SICKLE = 340;
const WEAPON_Y = 172;

var GameUI = //For UI logic specific to the in-game state
{
	weaponBob: 0.0,
	qHudBar: null,
	qWeapon: null,
	messageTimer: 0,
	messageText: "",
	messageQuad: null,
	qScrFlash: null,
	scrFlashTimer: 0,
	scrFlashFade: 0.0,
	init: function()
	{
		this.qHudBar = new Quad(Assets.getTexture("textures2/hud"), 64, -32, 512, 512);
		this.qWeapon = new Quad(Assets.getTexture("textures2/sickle_hud"), 340, 172, 512, 512);
		this.qWeapon.animated = true;
		this.qWeapon.animation = new Animation(256, 256, Assets.getTexture("textures2/sickle_hud"), 0.125);
		this.qWeapon.animation.frames = [0];
		this.qScrFlash = new Quad(Assets.getTexture("textures2/white"), 0, 0, 640, 480);
	},
	update: function(deltaTime)
	{
		//Animations of drawn quads are automatically updated by UI.
		if (this.messageTimer > 0.0) this.messageTimer -= deltaTime;
		if (this.scrFlashTimer > 0.0) 
		{
			this.scrFlashTimer = Math.max(0.0, this.scrFlashTimer - deltaTime);
			this.qScrFlash.colA -= deltaTime * this.scrFlashFade;
		}
	},
	draw: function()
	{
		if (this.scrFlashTimer > 0.0)
		{
			UI.quads.push(this.qScrFlash);
		}
		this.qWeapon.setPosition(WEAPON_X_SICKLE + Math.cos(this.weaponBob * 50.0) * 8.0,
			WEAPON_Y + Math.sin(this.weaponBob * 100.0) * 12.0);
		UI.quads.push(this.qWeapon);
		UI.quads.push(this.qHudBar);
		UI.drawText("FPS:" + gFps, 4, 4, 16);
		UI.drawText("X:" + gWorld.player.pos[0], 4, 20, 8);
		UI.drawText("Z:" + gWorld.player.pos[2], 4, 28, 8);
		UI.drawText("HP:" + gWorld.player.health, 72, 456, 16, 1.0, 0.0, 0.0);
		if (this.messageTimer > 0.0)
		{
			UI.drawText(this.messageText, 160, 120, 16);
		}
	},
	showMessage: function(text, timer)
	{
		this.messageText = text;
		this.messageTimer = timer;
	},
	flashScreen: function(r, g, b, a, duration)
	{
		this.scrFlashTimer = duration;
		this.qScrFlash.colR = r;
		this.qScrFlash.colG = g;
		this.qScrFlash.colB = b;
		this.qScrFlash.colA = a;
		this.scrFlashFade = a / duration;
	}
}

const UI = 
{
	font: null,
	quads: null, //List of quads that will be rendered for the next frame
	initialized: false,

	init: function()
	{
		this.font = Assets.getTexture("textures2/font");
		this.quads = [];
		this.quads.push(new Quad(Assets.getTexture("textures2/font"), 4, 4, 256, 256));
		this.initialized = true;
		
		DebugLog("UI INITIALIZED");
	},
	update: function(deltaTime)
	{
		for (var i = 0; i < this.quads.length; i++)
		{
			if (this.quads[i].animated && this.quads[i].animation && this.quads[i].visible) this.quads[i].animation.update(deltaTime);
		}
	},
	clear: function()
	{
		this.quads = [];
	},
	draw: function()
	{
		Assets.uiShader.bind();
		Assets.quadMesh.bind();
		for (var i = 0; i < this.quads.length; i++)
		{
			if (this.quads[i].visible) this.quads[i].draw();
		}
	},
	drawText: function(str, x, y, size, r, g, b)
	{
		size = size || 16;
		if (r === undefined) r = 1.0;
		if (g === undefined) g = 1.0;
		if (b === undefined) b = 1.0;
		str = str.toUpperCase();
		for (var i = 0; i < str.length; i++)
		{
			var chr = str.charCodeAt(i);
			if (chr > 127) chr = 0;
			chr += 6;
			var chrq = new Quad(UI.font, x + (i*size), y, size, size);
			chrq.sourceWidth = 0.125; //16 / 128
			chrq.sourceHeight = 0.0625; //16 / 256
			chrq.sourceX = (chr % 8) * chrq.sourceWidth;
			chrq.sourceY = (Math.floor((chr * 16.0) / 128.0) % 16) * chrq.sourceHeight;
			chrq.colR = r;
			chrq.colG = g;
			chrq.colB = b;
			chrq.colA = 1.0;
			this.quads.push(chrq);
		}
	}
};

function Quad(texture, x, y, width, height)
{
	this.visible = true;
	this.texture = texture;
	this.animated = false;
	this.animation = null;
	this.x = x;
	this.y = y;
	this.width = width;
	this.height = height;
	this.sourceX = 0.0;
	this.sourceY = 0.0;
	this.sourceWidth = 1.0;
	this.sourceHeight = 1.0;
	this.colR = 1.0;
	this.colG = 1.0;
	this.colB = 1.0;
	this.colA = 1.0;
	this.dirty = true;
	this.matrix = mat4.create();
}
Quad.prototype = 
{
	setPosition: function(x, y)
	{
		this.x = x;
		this.y = y;
		this.dirty = true;
	},
	setSize: function(w, h)
	{
		this.width = w;
		this.height = h;
		this.dirty = true;
	},
	draw: function()
	{
		if (this.dirty)
		{
			mat4.identity(this.matrix);
			var translation = vec3.fromValues(
				(2.0 * (this.x / gCanvas.clientWidth)) - 1.0, 
				-(2.0 * (this.y / gCanvas.clientHeight)) + 1.0, 
				0.0);
			mat4.translate(this.matrix, this.matrix, translation);
			var scaling = vec3.fromValues(
				(this.width / gCanvas.clientWidth) * 2.0, 
				-(this.height / gCanvas.clientHeight) * 2.0, 
				1.0);
			mat4.scale(this.matrix, this.matrix, scaling);
			this.dirty = false;
		}
		
		gl.bindTexture(gl.TEXTURE_2D, this.texture.id);
		gl.uniformMatrix4fv(Assets.uiShader.getUniformLocation("u_matrix"), false, this.matrix);
		if (this.animation != null)
		{
			gl.uniform2f(Assets.uiShader.getUniformLocation("u_sourcePos"), this.animation.sourceX, this.animation.sourceY);
			gl.uniform2f(Assets.uiShader.getUniformLocation("u_sourceSize"), this.animation.sourceWidth, this.animation.sourceHeight);
		}
		else
		{
			gl.uniform2f(Assets.uiShader.getUniformLocation("u_sourcePos"), this.sourceX, this.sourceY);
			gl.uniform2f(Assets.uiShader.getUniformLocation("u_sourceSize"), this.sourceWidth, this.sourceHeight);
		}
		gl.uniform4f(Assets.uiShader.getUniformLocation("u_color"), this.colR, this.colG, this.colB, this.colA);
		gl.uniform1i(Assets.uiShader.getUniformLocation("u_texture"), this.texture.id);
		
		gl.drawElements(gl.TRIANGLES, Assets.quadMesh.indexCount, gl.UNSIGNED_BYTE, 0);
	}
}