function World()
{
	this.entities = [];
	this.player = new Player(0.0, 0.0, 7.0);
	this.addEntity(this.player);
	this.camera = new Camera();
	this.map = null;
}
World.prototype = 
{
	loadMap: function(episode, map)
	{
		this.map = new GameMap();
		this.map.load(episode, map);
	},
	addEntity: function(ent)
	{
		this.entities.push(ent);
	},
	removeEntity: function(ent)
	{
		const idx = this.entities.indexOf(ent);
		if (idx !== -1) 
			this.entities.splice(idx, 1);
		else
			DebugLog("TRIED TO REMOVE ENTITY THAT WAS NOT THERE");
	},
	update: function(deltaTime)
	{
		GameUI.update(deltaTime);
		for (var i = 0; i < this.entities.length; i++)
		{
			this.entities[i].update(deltaTime);
		}
		this.map.update(deltaTime);
	},
	draw: function(deltaTime)
	{
		Sprite.prototype.prepareRender();
		var visCount = 0;
		for (var i = 0; i < this.entities.length; i++)
		{
			this.entities[i].draw();
			if (Object.getPrototypeOf(this.entities[i]) === Prop.prototype)
			{
				if (this.entities[i].visible)
				{
					visCount++;
				}
			}
		}
		if (gKeyboard[81])
		{
			GameUI.showMessage("Visible Props: " + visCount, 1.0);
		}
		this.map.draw(Assets.wallShader);
		GameUI.draw();
	},
	clearEntities: function()
	{
		this.entities.splice(0);
	}
};